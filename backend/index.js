require('dotenv').config()
const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const cookieParser = require('cookie-parser');
var cors = require('cors')
const customAuthMiddleware = require('./src/middleware/custom-auth-middleware');

// controller imports
const userController = require('./src/controllers/user-controller');

// set up the Express App
const app = express();
const PORT = process.env.PORT || 3001;

// Express middleware that allows POSTing data
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

if(process.env.LOCALHOST) app.use(cors());

// use the cookie-parser to help with auth token,
// it must come before the customAuthMiddleware
app.use(cookieParser());
app.use(customAuthMiddleware);

// serve up the public folder with frontend

// directory references
app.use(express.static(path.join(__dirname, 'public')))

// hook up our controllers
app.use(userController);


// Requiring our models for syncing
const db = require('./src/models/index');

// sync our sequelize models and then start server
db.sequelize.sync(/*{force: true}*/).then(() => {
    // inside our db sync callback, we start the server.
    // this is our way of making sure the server is not listening
    // to requests if we have not yet made a db connection
    app.listen(PORT, () => {
        console.log(`App listening on PORT ${PORT}`);
    });
});