const bcrypt = require('bcrypt');

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    email: {
      type: DataTypes.STRING,
      unique: true,
      allowNull: false,
    },
    referrer: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    emailing: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  });

  User.associate = function ({ AuthToken }) {
    User.hasMany(AuthToken);
  };

  User.authenticate = async function(email, password) {

    const user = await User.findOne({ where: { email } });

    if (bcrypt.compareSync(password, user.password)) {
      return user.authorize();
    }

    throw new Error('invalid password');
  };

  User.prototype.authorize = async function () {
    const { AuthToken } = sequelize.models;
    const user = this;

    const authToken = await AuthToken.generate(this.id);

    await user.addAuthToken(authToken);

    return { user, authToken }
  };


  User.prototype.logout = async function (token) {
    sequelize.models.AuthToken.destroy({ where: { token } });
  };

  return User;
};
