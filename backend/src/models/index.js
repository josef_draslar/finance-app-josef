"use strict";
// require the node packages
const fs = require("fs");
const path = require("path");
const Sequelize = require("sequelize");

const sequelize = new Sequelize(process.env["DATABASE_URL"], {
    logging: true,
  define: {
    underscored: false,
    freezeTableName: false,
    charset: "utf8",
    dialectOptions: {
      collate: "utf8_general_ci"
    },
    timestamps: true
  },
});

sequelize
  .authenticate()
  .then(() => {
    console.log("Connection has been established successfully.");
  })
  .catch(err => {
    console.error("Unable to connect to the database:", err);
  });

// This gathers up all the model files we have yet to create, and
// puts them onto our db object, so we can use them in the rest
// of our application

// set a reference to this file's name so we can exclude it later
const basename = path.basename(__filename);

const db = {};

fs
  .readdirSync(__dirname)
  .filter(file => {
    return (
      file.indexOf(".") !== 0 && file !== basename && file.slice(-3) === ".js"
    );
  })
  .forEach(file => {
    const model = sequelize["import"](path.join(__dirname, file));
    db[model.name] = model;
  });

Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

// export the main sequelize package with an uppercase 'S' and
// our own sequelize instance with a lowercase 's'
db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
