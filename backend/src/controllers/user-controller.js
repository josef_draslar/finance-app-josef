const express = require("express");
const bcrypt = require("bcrypt");

const router = express.Router();
const { User } = require("../models");

router.post("/api/users", async (req, res) => {
  const hash = bcrypt.hashSync(req.body.password, 10);

  try {
    let user = await User.create({ ...req.body, password: hash });

    let data = await user.authorize();

    return res.json(data);
  } catch (err) {
    return res.status(400).send(err);
  }
});

router.post("/api/users/login", async (req, res) => {
  const { email, password } = req.body;

  if (!email || !password) {
    return res.status(400).send("Request missing email or password param");
  }

  try {
    let user = await User.authenticate(email, password);

    return res.json(user);
  } catch (err) {
    return res.status(400).send("invalid email or password");
  }
});

router.delete("/api/users/logout", async (req, res) => {
  const { user, cookies: { auth_token: authToken } } = req;

  if (user && authToken) {
    await req.user.logout(authToken);
    return res.status(204).send();
  }

  return res.status(400).send({ errors: [{ message: "not authenticated" }] });
});

router.get("/api/users", (req, res) => {
  if (req.user) {
    return res.send(req.user);
  }
  res.status(404).send({ errors: [{ message: "missing auth token" }] });
});

module.exports = router;
