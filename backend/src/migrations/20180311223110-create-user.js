'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('user', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
        email: {
            type: Sequelize.STRING,
            unique: true,
            allowNull: false,
        },
        referrer: {
            type: Sequelize.STRING,
            allowNull: true,
        },
        emailing: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: true
        },
        password: {
            type: Sequelize.STRING,
            allowNull: false,
        },
        createdAt: {
            allowNull: false,
            type: Sequelize.DATE
        },
        updatedAt: {
            allowNull: false,
            type: Sequelize.DATE
        }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('user');
  }
};