import * as actions from "../reducers/UserReducer";
import axios from "../../utils/Axios";

export const fetchAndLogInUser = ({user, authToken}, cb) => {
  return (dispatch) => {
    console.log("heee", user, authToken);
    const accessTokenStringified = JSON.stringify(authToken);
    window.localStorage.setItem("userTkn", accessTokenStringified);
    user && window.localStorage.setItem("userData", JSON.stringify(user));
    dispatch({
      type: actions.SET_USER_TOKEN,
      payload: authToken,
      user: user?user:null
    });
    if(!user)
      axios
        .get(
          "/api/users/" + authToken.userId + "?access_token=" + authToken.token
        )
        .then(({ data }) => {
          window.localStorage.setItem("userData", JSON.stringify(data));

          dispatch({
            type: actions.SET_USER_TOKEN,
            payload: authToken,
            user: data
          });

          cb && cb();
        });
  };
};

export const updateUser = (user, cb) => {
  return dispatch => {
    window.localStorage.setItem("userData", JSON.stringify(user));
    if (!user) return null;
    dispatch({
      type: actions.UPDATE_USER,
      payload: user
    });
    cb && cb();
  };
};

export const refetchUser = () => {
  return (dispatch, getState) => {
    const accessToken = getState().user.userToken;

    if (!accessToken) return;

    axios
      .get(
        "/api/users/" + accessToken.userId + "?access_token=" + accessToken.id
      )
      .then(({ data }) => {
        console.log("user fetched", data, accessToken);
        window.localStorage.setItem("userData", JSON.stringify(data));

        dispatch({
          type: actions.UPDATE_USER,
          payload: data
        });
      });
  };
};

export const logOutUser = () => {
  return dispatch => {
    window.localStorage.removeItem("userTkn");
    window.localStorage.removeItem("userData");

    dispatch({
      type: actions.LOG_OUT_USER
    });
  };
};