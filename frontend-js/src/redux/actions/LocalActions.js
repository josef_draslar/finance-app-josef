import {localActions} from "../reducers/LocalReducer";
import {fireError} from "../../utils/Utils";

export const fireLocalAction = (key) => dispatch => {
    if(Object.values(localActions).indexOf(key) === -1) {
        fireError("fireLocalAction called with unknown key: "+key)
    }

    dispatch({
        type: key
    })
};