import { safelyParseJSON } from "../../utils/Utils";
export const SET_USER_TOKEN = "SET_USER_TOKEN",
  OPEN_SIGN_IN = "OPEN_SIGN_IN",
  OPEN_SIGN_UP = "OPEN_SIGN_UP",
  UPDATE_USER = "UPDATE_USER",
  LOG_OUT_USER = "LOG_OUT_USER";
export const SIGN_IN = "SIGN_IN",
  SIGN_UP = "SIGN_UP",
  RESET_PASSWORD = "RESET_PASSWORD",
  CHANGE_PASSWORD = "CHANGE_PASSWORD";

const initialState = {
  userToken: safelyParseJSON(window.localStorage.getItem("userTkn"), null),
  user: safelyParseJSON(window.localStorage.getItem("userData"), null),
};

const user = (state = initialState, action) => {
  switch (action.type) {
    case SET_USER_TOKEN:
      return {
        ...state,
        userToken: action.payload,
        user: action.user
      };
    case UPDATE_USER:
      return {
        ...state,
        user: action.payload
      };
    case LOG_OUT_USER:
      return {
        ...state,
        user: null,
        userToken: null,
        favouriteRecipes: null,
        attendingEvents: null
      };
    default:
      return state;
  }
};

export default user;
