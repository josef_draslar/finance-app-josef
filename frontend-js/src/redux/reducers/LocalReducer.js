import {getUniqueId} from "../../utils/UniqueId";

export const localActions = Object.freeze({
    OPEN_SIGN_IN: getUniqueId(),
    OPEN_SIGN_UP: getUniqueId(),
    CLOSE_JOIN: getUniqueId(),
    OPEN_RESET_PASSWORD: getUniqueId(),
    OPEN_CHANGE_PASSWORD: getUniqueId()
});

const SIGN_IN = "SIGN_IN",
  SIGN_UP = "SIGN_UP",
  RESET_PASSWORD = "RESET_PASSWORD",
  CHANGE_PASSWORD = "CHANGE_PASSWORD";

const initialState = {
  joinModal: null,
};

const actionsHandler = (state = initialState, action) => {
  switch (action.type) {
    case localActions.OPEN_SIGN_IN:
      return {
        ...state,
        joinModal: SIGN_IN
      };
    case localActions.OPEN_SIGN_UP:
      return {
        ...state,
        joinModal: SIGN_UP
      };
    case localActions.OPEN_RESET_PASSWORD:
      return {
        ...state,
        joinModal: RESET_PASSWORD
      };
    case localActions.OPEN_CHANGE_PASSWORD:
      return {
        ...state,
        joinModal: CHANGE_PASSWORD
      };
    case localActions.CLOSE_JOIN:
      return {
        ...state,
        joinModal: null
      };
    default:
      return state;
  }
};

export default actionsHandler;
