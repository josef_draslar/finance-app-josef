import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Container, Navbar, NavbarBrand } from "reactstrap";
import { Link } from "react-router-dom";
import styled from "styled-components";
import colors from "../utils/colors";
import { media } from "../utils/Media";
import {
  NavLinkStyled,
  NavItemMainLocal,
  NavItemMobileLocal
} from "../components/MenuStyledComponents";
import { logOutUser } from "../redux/actions/UserActions";
import { fireLocalAction } from "../redux/actions/LocalActions";
import Join from "../components/Join";
import { setNotificationMessageCallback } from "../components/NotificationMessage";
import NotificationMessage from "../components/NotificationMessage";
import NavMobile from "./NavMobile";
import NavMain from "./NavMain";
import { getImageUrl } from "../utils/Utils";
import { localActions } from "../redux/reducers/LocalReducer";
import { withTranslation } from "../utils/multiLanguageSupport";

class NavBar extends React.PureComponent {
  state = {
    isOpen: false,
    notificationMessage: null,
    fixed: false
  };

  displayNotificationMessage = message => {
    this.setState({
      notificationMessage: <NotificationMessage messageObject={message} />
    });
    setTimeout(() => this.setState({ notificationMessage: null }), 15000);
  };

  componentDidMount() {
    window.document.getElementById("loading-placeholder").style.display =
      "none";
    window.document.getElementById("loading-placeholder-bcg").style.display =
      "none";

    setNotificationMessageCallback(this.displayNotificationMessage);

    document.addEventListener("scroll", this.scroll);
  }

  componentWillUnmount() {
    document.addEventListener("scroll", undefined);
  }

  scroll = () => {
    const browserHeight =
      (isNaN(window.innerHeight) ? window.clientHeight : window.innerHeight) ||
      document.documentElement
        ? document.documentElement.clientHeight
        : 0;
    const scrollFromTop =
      document.body.scrollTop || document.documentElement.scrollTop;
    const { fixed } = this.state;

    const comparison = scrollFromTop - browserHeight;
    if (comparison > 0 && !fixed) {
      this.setState({ fixed: true });
    } else if (comparison <= 0 && fixed) {
      this.setState({ fixed: false });
    }
  };

  toggle = () => {
    this.setState({
      isOpen: !this.state.isOpen
    });
  };

  getNavLinkMobile = (url, name, main) =>
    <NavItemMobileLocal>
      <NavLinkStyled
        main={main ? 1 : 0}
        tag={Link}
        to={url}
        className={url === this.props.location.pathname ? "isActive" : ""}
      >
        {this.props.t(name)}
      </NavLinkStyled>
    </NavItemMobileLocal>;

  getNavLink = (url, name, mobileHide, main, pathname) =>
    <NavItemMainLocal mobilehide={mobileHide ? 1 : 0}>
      <NavLinkStyled
        main={main ? 1 : 0}
        tag={Link}
        to={url}
        className={url === pathname ? "isActive" : ""}
      >
        {this.props.t(name)}
      </NavLinkStyled>
    </NavItemMainLocal>;

  openSignIn = () => {
    this.toggle();

    this.props.fireLocalAction(localActions.OPEN_SIGN_IN);
  };

  openSignUp = () => {
    this.toggle();
    this.props.fireLocalAction(localActions.OPEN_SIGN_UP);
  };

  logOutUser = () => {
    this.toggle();
    this.props.logOutUser();
  };

  render() {
    const { isOpen, notificationMessage, fixed } = this.state;
    const {
      userToken,
      location,
      navBarWhite,
      showHeaderShadow,
      showBorder,
      t
    } = this.props;
    return (
      <Wrapper>
        <NavBarWrapper>
          <NavbarStyled
            fixed={fixed}
            showBorder={showBorder}
            showHeaderShadow={showHeaderShadow}
          >
            <ContainerLocal>
              <NavBarBrandStyled tag={Link} to="/">
                <LogoBars src={getImageUrl("static/logo.svg")} />
              </NavBarBrandStyled>
              <NavMain
                userToken={userToken}
                navBarWhite={!fixed && navBarWhite}
                openSignUp={this.props.openSignUp}
                openSignIn={this.props.openSignIn}
                logOutUser={this.props.logOutUser}
                location={location}
                t={t}
              />
              <MenuBars
                onClick={this.toggle}
                className={isOpen ? "isOpen" : ""}
              >
                <Bar
                  navBarWhite={
                    !fixed && navBarWhite ? colors.white : colors.dark
                  }
                />
                <Bar
                  navBarWhite={
                    !fixed && navBarWhite ? colors.white : colors.dark
                  }
                />
                <Bar
                  navBarWhite={
                    !fixed && navBarWhite ? colors.white : colors.dark
                  }
                />
              </MenuBars>
              <NavMobile
                isOpen={isOpen}
                userToken={userToken}
                openSignIn={this.openSignIn}
                openSignUp={this.openSignUp}
                logOutUser={this.logOutUser}
                location={location}
                toggle={this.toggle}
                t={t}
              />
            </ContainerLocal>
          </NavbarStyled>
        </NavBarWrapper>
        <Container>
          <Join />
          {notificationMessage && notificationMessage}
        </Container>
      </Wrapper>
    );
  }
}

NavBar.propTypes = {
  location: PropTypes.object.isRequired,
  navBarWhite: PropTypes.bool,
  showBorder: PropTypes.bool,
  showHeaderShadow: PropTypes.bool
};

export default withTranslation(
  connect(
    state => ({
      userToken: state.user.userToken
    }),
    {
      logOutUser,
      fireLocalAction
    }
  )(NavBar)
);

const NavBarBrandStyled = styled(NavbarBrand)`
  height: 65px !important;
  padding-top: 0;
  color: ${colors.white};
  font-size: 35px;
  font-weight: 600;
  position: relative;

  ${media.lessThan("md")`
    margin-left: auto;
    margin-right: auto;
  `};
`;

const NavBarWrapper = styled.div`
  z-index: 101;
  padding: 0;
  height: 106px;
  position: relative;

  ${media.lessThan("md")`height: 69px;`};
`;

const NavbarStyled = styled(
  ({ fixed, showBorder, showHeaderShadow, ...rest }) => <Navbar {...rest} />
)`
  z-index: 101;
  padding: 0;
  height: 100%;
  position: unset;
  top: -106px;
  min-width: 320px;

  -webkit-transition: top 0.5s linear;
  -moz-transition: top 0.5s linear;
  transition: top 0.5s linear;

  ${props =>
    props.showHeaderShadow &&
    `box-shadow: 0 0 13px 3px rgba(0,0,0,.05);`} ${props =>
  props.fixed &&
  `
      position: fixed;
     top: 0;
    left: 0;
    right: 0;
    height: 70px;
    background: ${colors.white};
    box-shadow: 0 0 13px 3px rgba(0,0,0,.05);
  `};

  ${props =>
    props.showBorder && `border-bottom: 1px solid ${colors.lightestGray};`};
`;

const Wrapper = styled.div`background: ${colors.white};`;

const LogoBars = styled.img`
  height: 65px;
  width: 170px;
`;

const Bar = styled.div`
  background-color: ${props => props.color};
  position: absolute;
  width: 100%;
  height: 2px;
  border-radius: 1px;

  -webkit-transition: all 0.3s ease-out;
  -moz-transition: all 0.3s ease-out;
  transition: all 0.3s ease-out;

  &:first-child {
    top: 0;
  }

  &:nth-child(2) {
    top: 6px;
  }

  &:last-child {
    bottom: 0;
  }

  ${media.lessThan("sm")`
    background-color: ${colors.dark};
  `};
`;

const MenuBars = styled.div`
  width: 17px;
  height: 14px;
  position: absolute;
  right: 10px;
  z-index: 1;

  &:hover,
  &:focus {
    cursor: pointer;
  }

  &.isOpen {
    ${Bar}:first-child {
      transform: translate(-90px) rotate(45deg);
      ${media.lessThan("md")`
        transform: translate(90px) rotate(45deg);
      `};
      opacity: 0;
    }

    ${Bar}:nth-child(2) {
      opacity: 0;
    }

    ${Bar}:last-child {
      transform: translate(-90px) rotate(-45deg);
      ${media.lessThan("md")`
        transform: translate(90px) rotate(-45deg);
      `};
      opacity: 0;
    }
  }

  ${media.lessThan("md")`
    right: auto;
    left: 10px;
  `};
`;

const ContainerLocal = styled(Container)`
  position: relative;
  height: 100%;

  ${media.lessThan("355px")`
        padding-left: 30px;
  `};
`;
