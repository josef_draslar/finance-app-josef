import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import styled from "styled-components";
import colors from "../utils/colors";
import { media } from "../utils/Media";
import {
  NavItemMainLocal,
  NavLinkBase
} from "../components/MenuStyledComponents";
import fonts from "../utils/fonts";
import {withTranslation} from "../utils/multiLanguageSupport";

const getNavLinkOnClickInnerUl = (name, onClick) => (
  <NavItemMainLocalInner>
    <NavLinkStyledInner onClick={onClick}>{name}</NavLinkStyledInner>
  </NavItemMainLocalInner>
);

const getNavLinkInnerUl = (url, name, pathname, onClick) => (
  <NavItemMainLocalInner>
    <NavLinkStyledInner
      main={1}
      tag={Link}
      to={url}
      onClick={onClick}
      className={url === pathname ? "isActive" : ""}
    >
      {name}
    </NavLinkStyledInner>
  </NavItemMainLocalInner>
);

class NavInnerUl extends React.PureComponent {
  render() {
    const { links, isOpen, width } = this.props;
    return (
      <InnerUl isOpen={isOpen} width={width}>
        {links.map(
          (link, key) =>
            link.url
              ? getNavLinkInnerUl(
                  link.url,
                this.props.t("NavBar." + link.name),
                  this.props.location.pathname,
                  link.onClick
                )
              : getNavLinkOnClickInnerUl(this.props.t("NavBar." + link.name), link.onClick)
        )}
      </InnerUl>
    );
  }
}

NavInnerUl.propTypes = {
  isOpen: PropTypes.func.isRequired,
  location: PropTypes.object.isRequired,
  width: PropTypes.number,
  links: PropTypes.arrayOf(PropTypes.object).isRequired
};

export default withTranslation(NavInnerUl);

const NavItemMainLocalInner = styled(NavItemMainLocal)`
  display: block;
  position: relative;
  float: none;
  width: 100%;
  ${media.lessThan("615px")`
      display: none;
  `};
`;

const NavLinkStyledInner = styled(NavLinkBase)`
  font-family: ${fonts.ubuntu};
  font-size: 12px;
  text-transform: uppercase;

  display: block;
  float: none;
  margin: 0;
  padding: 9px 40px;

  letter-spacing: 0.15em;
  line-height: 20px;
  font-weight: 400;

  color: ${colors.dark} !important;

  &:hover,
  &:focus {
    text-decoration: none !important;
  }

  &:hover,
  &:focus {
    color: ${colors.light} !important;
    cursor: pointer;
  }

  &.isActive {
    color: ${colors.light} !important;
  }

  &.isActive {
    text-decoration: none !important;
  }
`;
