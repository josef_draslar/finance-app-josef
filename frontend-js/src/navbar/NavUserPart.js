import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import colors from "../utils/colors";
import { getImageUrl } from "../utils/Utils";
import {
  NavigationItem,
  NavItemMainLocal
} from "../components/MenuStyledComponents";
import { connect } from "react-redux";
import { fireLocalAction } from "../redux/actions/LocalActions";
import { localActions } from "../redux/reducers/LocalReducer";
import { logOutUser } from "../redux/actions/UserActions";
import { media } from "../utils/Media";

class NavUserPart extends React.PureComponent {
  state = {
    isOpen: false,
    isHovered: false
  };

  openClose = () => this.setState({ isOpen: !this.state.isOpen });

  hover = () => this.setState({ isHovered: true });

  blur = () => this.setState({ isHovered: false });

  logOutUser = () => {
    this.props.logOutUser();
    this.closeModal();
  };

  openSignIn = () => {
    this.props.fireLocalAction(localActions.OPEN_SIGN_IN);
    this.closeModal();
  };

  openSignUp = () => {
    this.props.fireLocalAction(localActions.OPEN_SIGN_UP);
    this.closeModal();
  };

  closeModal = () => this.setState({ isOpen: false });

  render() {
    const { userToken, location } = this.props;
    const { isOpen, isHovered } = this.state;
    const ulOpened = isOpen ? true : isHovered;

    return (
      <NavItemMainLocalLocal
        onMouseEnter={this.hover}
        onMouseLeave={this.blur}
        hideForMobile
      >
        <ImageWrapper isOpen={ulOpened} onClick={this.openClose}>
          <Image
            src={getImageUrl(
              userToken ? "static/avatar-black.png" : "static/avatar.png"
            )}
            alt="avatar"
          />
        </ImageWrapper>

        <InnerUl isOpen={ulOpened}>
          {userToken
            ? <NavigationItem
                url="/uzivatel/profil"
                name="user"
                onClick={this.closeModal}
              />
            : ""}
          {userToken
            ? <NavigationItem name="logOut" onClick={this.logOutUser} />
            : ""}
        </InnerUl>
      </NavItemMainLocalLocal>
    );
  }
}

NavUserPart.propTypes = {
  userToken: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired
};

export default connect(null, {
  fireLocalAction,
  logOutUser
})(NavUserPart);

const Image = styled.img`
  top: 50%;
  margin-top: -19px;
  display: block;
  position: relative;
  height: 32px;
  border: 1px solid ${colors.dark};
  border-radius: 25px;
`;

const ImageWrapper = styled.div`
  position: relative;
  padding: 0 20px;
  height: 100%;

  &:hover ${Image}, &:focus ${Image} {
    cursor: pointer;
    border: 1px solid ${colors.light};
  }

  & ${Image} {
    ${props =>
      props.isOpen &&
      `
        border: 1px solid ${colors.light};
        border-radius: 25px;
        `};
  }
`;

const NavItemMainLocalLocal = styled(NavItemMainLocal)`
  height: 100%;
`;

const InnerUl = styled.ul`
  position: absolute;
  display: block;
  left: -18px;
  top: 100%;
  z-index: 10;
  -webkit-transition: opacity 0.15s ease-out;
  -moz-transition: opacity 0.15s ease-out;
  transition: opacity 0.15s ease-out;
  background-color: ${colors.white};
  padding: 24px 24px 0 24px;
  width: 150px;

  -webkit-box-shadow: 0px 10px 12px 0px rgba(0, 0, 0, 0.1);
  -moz-box-shadow: 0px 10px 12px 0px rgba(0, 0, 0, 0.1);
  box-shadow: 0px 10px 12px 0px rgba(0, 0, 0, 0.1);

  ${props => !props.isOpen && `opacity: 0; top: -200%;`};
`;
