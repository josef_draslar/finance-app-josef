import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { media } from "../utils/Media";
import { NavigationItem } from "../components/MenuStyledComponents";
import colors from "../utils/colors";
import fonts from "../utils/fonts";

class NavMobile extends React.PureComponent {
  stopPropagation = e => e.stopPropagation();

  refresh = () => window.location.reload();

  render() {
    const {
      isOpen,
      userToken,
      openSignIn,
      openSignUp,
      logOutUser,
      toggle
    } = this.props;

    return (
      <div>
        <BackDrop className={isOpen ? "isOpened" : ""} onClick={toggle} />
        <CollapseStyled
          className={isOpen ? "isOpened" : ""}
          navbar
          onClick={this.stopPropagation}
        >
          <CrossBars onClick={toggle} className={isOpen ? "" : "isClosed"}>
            <CrossBar />
            <CrossBar />
          </CrossBars>
          <Ul>
            {userToken
              ? <NavigationItem
                  name="user"
                  url="/uzivatel/profil"
                  onClick={toggle}
                  isMobile
                  isInlined
                />
              : <NavigationItem name="signIn" onClick={openSignIn} isMobile isInlined/>}
            {userToken
              ? <NavigationItem name="logOut" onClick={logOutUser} isMobile isInlined/>
              : <NavigationItem name="signUp" onClick={openSignUp} isMobile isInlined/>}

            <Cb />
            <Title>MENU</Title>

            <NavigationItem url="/" name="main" onClick={toggle} isMobile isOpen={isOpen}/>
            <NavigationItem url="/history" name="history" onClick={toggle} isMobile isOpen={isOpen}/>
          </Ul>
          <Refresh onClick={this.refresh}>
            <i className="fas fa-redo" /> OBNOVIT
          </Refresh>
        </CollapseStyled>
      </div>
    );
  }
}

NavMobile.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  userToken: PropTypes.object,
  location: PropTypes.object.isRequired,
  openSignUp: PropTypes.func.isRequired,
  toggle: PropTypes.func.isRequired,
  openSignIn: PropTypes.func.isRequired,
  logOutUser: PropTypes.func.isRequired
};

export default NavMobile;

const CollapseStyled = styled.div`
  -webkit-transition: all 0.3s ease-out;
  -moz-transition: all 0.3s ease-out;
  transition: all 0.3s ease-out;

  background-color: ${colors.white};
  max-width: 360px;
  position: fixed;
  left: -90%;
  top: 0;
  bottom: 0;
  width: 90%;

  padding: 24px 0;

  &.isOpened {
    left: 0;
  }
`;

const BackDrop = styled.div`
  position: fixed;
  display: none;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  opacity: 0;
  background-color: ${colors.light};

  -webkit-transition: opacity 2s ease-out;
  -moz-transition: opacity 2s ease-out;
  transition: opacity 2s ease-out;

  &.isOpened {
    display: block;
    opacity: 0.95;
  }

  &:hover {
    cursor: pointer;
  }
`;

const CrossBar = styled.div`
  background-color: ${colors.dark};
  position: absolute;
  height: 2px;
  border-radius: 1px;
  width: 50%;
  top: 20px;
  left: 12px;

  &:first-child {
    transform: rotate(-45deg);
  }

  &:last-child {
    transform: rotate(45deg);
  }
`;

const CrossBars = styled.div`
  width: 40px;
  height: 40px;
  top: 15px;
  right: 15px;
  position: absolute;
  z-index: 1;
  ${media.lessThan("md")`
    top: 25px;
  `};

  -webkit-transition: opacity 0.3s ease-out;
  -moz-transition: opacity 0.3s ease-out;
  transition: opacity 0.3s ease-out;

  &:hover,
  &:focus {
    cursor: pointer;
  }

  &.isClosed {
    ${CrossBar}:first-child {
      opacity: 0;
    }

    ${CrossBar}:last-child {
      opacity: 0;
    }
  }

  display: block;
`;

const Refresh = styled.div`
  bottom: 15px;
  left: 15px;
  position: absolute;
  z-index: 1;
  color: ${colors.dark};

  font-size: 17px;
  line-height: 23px;
  font-family: ${fonts.mainTitle};

  &:hover,
  &:focus {
    cursor: pointer;
  }

  i {
    padding-right: 10px;
  }
`;

const Title = styled.div`
  font-size: 12px;
  position: relative;
  margin-top: 20px;
  margin-bottom: 10px;
  list-style: none;
  color: rgba(153, 153, 153, 0.5);
  width: 100%;
  letter-spacing: 3px;
  text-transform: uppercase;
  font-weight: 600;
  padding-left: 24px;
`;

const Ul = styled.ul`
  list-style: none;
  padding-right: 50px;
  margin-left: 0;
  padding-left: 10px;
`;

const Cb = styled.div`clear: both;`;
