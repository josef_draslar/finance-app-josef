import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { media } from "../utils/Media";
import {connect} from "react-redux";
import {fireLocalAction} from "../redux/actions/LocalActions";
import {localActions} from "../redux/reducers/LocalReducer";
import {withTranslation} from "../utils/multiLanguageSupport";
import {NavigationItem} from "../components/MenuStyledComponents";
import NavUserPart from "./NavUserPart";

class NavMain extends React.PureComponent {
  signIn = () => this.props.fireLocalAction(localActions.OPEN_SIGN_IN);

  render() {
    const {userToken,location} = this.props;
    return (
      <Ul>
          <NavigationItem url="/" name="main" hideForMobile />
          <NavigationItem url="/history" name="history" hideForMobile />
          {!!userToken &&
              <NavUserPart
                  userToken={userToken}
                  location={location}
              />
          }
          <NavigationItem onClick={this.signIn} name="signIn" isPrimary />
      </Ul>
    );
  }
}

NavMain.propTypes = {
  userToken: PropTypes.object,
  location: PropTypes.object.isRequired,
  navBarWhite: PropTypes.bool
};

export default withTranslation(connect(null,{fireLocalAction})(NavMain))

const Ul = styled.ul`
  & li {
    display: inline;
    float: left;
  }
  margin-bottom: 0;
  height: 100%;

  ${media.lessThan("md")`
    padding-left: 0;
  `};
`;