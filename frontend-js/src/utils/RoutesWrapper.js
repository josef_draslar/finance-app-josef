import React from "react";
import PropTypes from "prop-types";

import NavBar from "../navbar/NavBar";
import IphoneAddToHp from "../components/IphoneAddToHp";
import bugsnagClient from "../utils/BugsnagClient";

class FallBackScreen extends React.PureComponent {
  render() {
    window.location.href = "/";
    return null;
  }
}

class RoutesWrapper extends React.PureComponent {
  render() {
    const ErrorBoundary = bugsnagClient.getPlugin("react");
      //TODO add bugsnag beforesend callback
    return (
      <ErrorBoundary fallbackComponent={FallBackScreen}>
        <div style={{ height: "100%" }}>
          <IphoneAddToHp />
          {!this.props.hideNavBar && (
            <NavBar
              location={this.props.location}
              navBarWhite={this.props.navBarWhite}
              showHeaderShadow={this.props.showHeaderShadow}
              showBorder={this.props.showBorder}
            />
          )}
          {this.props.children}
        </div>
      </ErrorBoundary>
    );
  }
}

RoutesWrapper.propTypes = {
  location: PropTypes.object.isRequired,
  hideNavBar: PropTypes.bool,
  navBarWhite: PropTypes.bool,
  showHeaderShadow: PropTypes.bool,
  showBorder: PropTypes.bool
};

RoutesWrapper.defaultProps = {
  hideNavBar: false
};

export default RoutesWrapper;