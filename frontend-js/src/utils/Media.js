import { generateMedia } from "styled-media-query";

export const media = generateMedia({
  xxs: "400px", //iphone5 and small androids
  mobileLayout: "575px",
  sm: "767px",
  md: "991px",
  lg: "1199px"
});
