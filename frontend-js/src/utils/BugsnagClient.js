import React from "react";
import bugsnag from "@bugsnag/js";
import bugsnagReact from "@bugsnag/plugin-react";

const bugsnagClient = bugsnag("93d5aa4d16683cdb39bdc9aa48b03e25");
bugsnagClient.use(bugsnagReact, React);

export default bugsnagClient;