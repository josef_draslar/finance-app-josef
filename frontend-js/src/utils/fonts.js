const fonts = {
  mainTitle: "Teko, sans-serif",
  title: "Montserrat, sans-serif",
  text: "Ubuntu, sans-serif"
};

export default fonts;
