import axios from "axios";

export default axios.create({
    headers: {
        'Access-Control-Allow-Origin': '*',
    },
    baseURL: window.location.hostname==='localhost'?'http://localhost:3001/api/':'/api/',
    timeout: 1000
});