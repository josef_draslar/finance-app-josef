import React from "react";
import { fireError } from "./Utils";
import axios from "axios";
import defaultTranslations from "../static/translation.json";

let listeners = [];

const subscribeListener = listener => listeners.push(listener);

const unsubscribeListener = l =>
  (listeners = listeners.filter(listener => l !== listener));

const notifyListeners = () => listeners.forEach(l => l());

const fallbackLanguage = "cs";

const userLanguage =
  (navigator.languages && navigator.languages[0]) ||
  navigator.language ||
  navigator.userLanguage ||
  fallbackLanguage;

let translations = defaultTranslations;

const setTranslations = tran => {
  translations = tran;
  notifyListeners();
};

export const fetchTranslations = () =>
  axios
    .get("/locales/" + userLanguage.split("-")[0] + "/translation.json")
    .then(({ data }) => setTranslations(data))
    .catch(() =>
      axios
        .get("/locales/" + fallbackLanguage + "/translation.json")
        .then(({ data }) => setTranslations(data))
        .catch(() => fireError("not able to default fetch language file"))
    );

export const withTranslation = WrappedComponent => {
  return class extends React.PureComponent {
    componentDidMount() {
      subscribeListener(this.handleChange);
    }

    componentWillUnmount() {
      unsubscribeListener(this.handleChange);
    }

    handleChange = () => this.forceUpdate();

    render() {
      const getLocalizedMessage = key => {
        let value;
        try {
          value = key.split(".").reduce((o, i) => o[i], translations);
        } catch (e) {}
        if (!value) {
          fireError("localized message not found with key: " + key);
          return key;
        }
        return value;
      };

      return <WrappedComponent t={getLocalizedMessage} {...this.props} />;
    }
  };
};
