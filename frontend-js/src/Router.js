import React from "react";
import { Router, Route, Switch, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import history from "./utils/History";
import HomePage from "./pages/HomePage";
import HistoryPage from "./pages/HistoryPage";
import {
  getParameterFromUrl,
  removeParamsFromUrl,
  trackPageView
} from "./utils/Utils";
import {
  refetchUser
} from "./redux/actions/UserActions";
import {
    fireLocalAction
} from "./redux/actions/LocalActions";
import RoutesWrapper from "./utils/RoutesWrapper";
import PrivacyPolicy from "./pages/static/PrivacyPolicy";
import TermsAndConditions from "./pages/static/TermsAndConditions";
import ForceUserLogIn from "./pages/static/ForceUserLogIn";
import UserAccount from "./pages/user/UserAccount";
import { pushNotificationMessageIfPossible } from "./components/NotificationMessage";
import {localActions} from "./redux/reducers/LocalReducer";

history.listen(location => {
  trackPageView(location.pathname + location.search);
  window.scrollTo(0, 0);
});

const RouterComponent = ({ t, fireLocalAction, refetchUser }) => (
  <Router history={history}>
    <div style={{ height: "100%" }}>
      <Switch>
        <Route
          exact
          path="/"
          render={({ location }) => {
            const action = getParameterFromUrl("action");
            if (action) {
              removeParamsFromUrl("action");

              switch (action) {
                case "registration_verified":
                  setTimeout(
                    () =>
                      pushNotificationMessageIfPossible({
                        title: "Email ověřen.",
                        message: "Nyní se můžeš přihlásit.",
                        type: "SUCCESS"
                      }),
                    1500
                  );
                  break;
                default:
                  break;
              }
            }

            return (
              <RoutesWrapper showBorder location={location}>
                <HomePage />
              </RoutesWrapper>
            );
          }}
        />
        <Route
          exact
          path="/history"
          render={({ location }) => {
            return (
              <RoutesWrapper showBorder location={location}>
                <HistoryPage />
              </RoutesWrapper>
            );
          }}
        />
        <Route
          exact
          path="/podminky-osobnich-udaju"
          render={({ location }) => (
            <RoutesWrapper showHeaderShadow location={location}>
              <PrivacyPolicy />
            </RoutesWrapper>
          )}
        />
        <Route
          exact
          path="/vseobecne-obchodni-podminky"
          render={({ location }) => (
            <RoutesWrapper showHeaderShadow location={location}>
              <TermsAndConditions />
            </RoutesWrapper>
          )}
        />
        <Route
          exact
          path="/uzivatel/profil"
          render={({ location }) => (
            <RoutesWrapper showHeaderShadow location={location}>
              <ForceUserLogIn>
                <UserAccount />
              </ForceUserLogIn>
            </RoutesWrapper>
          )}
        />
        <Route
          exact
          path="/prihlaseni"
          render={({ location }) => {
              fireLocalAction(localActions.OPEN_SIGN_IN);
            return (
              <RoutesWrapper showBorder location={location}>
                <HomePage />
              </RoutesWrapper>
            );
          }}
        />
        <Route
          exact
          path="/registrace"
          render={({ location }) => {
              fireLocalAction(localActions.OPEN_SIGN_UP);
            return (
              <RoutesWrapper showBorder location={location}>
                <HomePage />
              </RoutesWrapper>
            );
          }}
        />
        <Route
          exact
          path="/withparameters"
          strict
          render={({ match, location }) => (
            <RoutesWrapper location={location} navBarWhite>
              <HomePage
                innerSectionUrl={match.params.innerSectionUrl}
                tabSection={match.params.tabSection}
              />
            </RoutesWrapper>
          )}
        />
        <Redirect to="/" path="/" />
      </Switch>
    </div>
  </Router>
);

export default connect(null, {
  fireLocalAction,
  refetchUser
})(RouterComponent);
