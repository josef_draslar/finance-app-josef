import React from "react";
import { connect } from "react-redux";
import styled from "styled-components";

import PersonalDetails from "./PersonalDetails";
import { updateUser } from "../../redux/actions/UserActions";
import { fireLocalAction } from "../../redux/actions/LocalActions";
import ForceUserLogIn from "../static/ForceUserLogIn";
import Switcher from "../../components/Switcher";
import {localActions} from "../../redux/reducers/LocalReducer";

class UserAccount extends React.PureComponent {
  state = {
    chosen: 0
  };

  switch = chosen => this.setState({ chosen });

  render() {
    const {
      user,
      userToken,
      updateUser
    } = this.props;
    const { chosen } = this.state;
    if (!user || !user.email) return null;
    return (
      <div>
        <SwitcherWrapper>
          <Switcher
            chosen={chosen}
            choose={this.switch}
            options={["Nastavení"]}
          />
        </SwitcherWrapper>
        {chosen === 0 ? (
          <PersonalDetails
            user={user}
            userToken={userToken}
            updateUser={updateUser}
            openPasswordReset={()=>this.props.fireLocalAction(localActions.OPEN_RESET_PASSWORD)}
          />
        ) : (
          ""
        )}
      </div>
    );
  }
}

class UserAccountSignInWrapper extends React.PureComponent {
  render() {
    return (
      <ForceUserLogIn>
        <UserAccount {...this.props} />
      </ForceUserLogIn>
    );
  }
}

export default connect(
  state => ({
    user: state.user.user,
    userToken: state.user.userToken
  }),
  {
    updateUser,
      fireLocalAction
  }
)(UserAccountSignInWrapper);

const SwitcherWrapper = styled.div`
  margin-top: 30px;
  margin-bottom: 25px;
`;
