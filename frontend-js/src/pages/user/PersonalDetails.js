import React from "react";
import PropTypes from "prop-types";
import {
  Container,
  Form,
  FormFeedback,
  FormGroup,
  Input,
  Label
} from "reactstrap";
import axios from "../../utils/Axios";
import styled from "styled-components";

import fonts from "../../utils/fonts";
import { pushNotificationMessageIfPossible } from "../../components/NotificationMessage";
import Button from "../../components/Button";
import {ButtonLikeLink} from "../../components/BasicElements";

//TODO finish implementation
class PersonalDetails extends React.PureComponent {
  state = {
    email: this.props.user.email,
    emailing: this.props.user.emailingAllowed,
    isLoading: false,
    errors: {},

    isLoadingUser: false,
    doneUser: false,
    name: this.props.user ? this.props.user.name : "",
    surname: this.props.user ? this.props.user.surname : "",
    street: this.props.user ? this.props.user.street : "",
    city: this.props.user ? this.props.user.city : "",
    psc: this.props.user ? this.props.user.psc : "",
    state: this.props.user ? this.props.user.state : "Česká Republika"
  };

  onChange = e => {
    const { name, value, type } = e.target;

    this.setState({
      [name]: type === "checkbox" ? !this.state[name] : value,
      errors: {
        ...this.state.errors,
        [name]: null
      },
      info: ""
    });
  };

  onSubmitUser = e => {
    e.preventDefault();
    const { street, city, psc, name, surname, state } = this.state;
    const { userToken } = this.props;

    this.setState({ isLoadingUser: true });
    const data = {
      street,
      city,
      psc,
      name,
      surname,
      state
    };
    axios
      .patch(
        "/api/users/" + userToken.userId + "?access_token=" + userToken.id,
        data
      )
      .then(() => {
        this.props.updateUser({ ...this.props.user, ...data }, () =>
          this.setState({ doneUser: true, isLoadingUser: false }, () =>
            setTimeout(() => this.setState({ doneUser: false }), 4000)
          )
        );
      })
      .catch(() => this.setState({ isLoadingUser: false }));
  };

  onSubmit = e => {
    e.preventDefault();
    const { email, emailing } = this.state;
    const { userToken, updateUser } = this.props;
    this.setState({ isLoading: true });
    console.log("emailing", emailing);
    axios
      .post(
        "/api/users/updateCustom?id=" +
          userToken.userId +
          "&access_token=" +
          userToken.id,
        {
          email,
          emailingAllowed: emailing
        }
      )
      .then(({ data }) => {
        this.setState({ email: data.result.user.email, isLoading: false });
        updateUser(data.result.user);
        if (email !== this.props.user.email) {
          pushNotificationMessageIfPossible({
            title: "Email úspěšně změněn",
            message:
              "Zkontrolujte svůj email a klikněte a verifikační odkaz, pro ověření emailu, před tím než se znovu přihlásíte.",
            type: "SUCCESS"
          });
        } else {
          pushNotificationMessageIfPossible({
            message: "Uloženo",
            type: "SUCCESS"
          });
        }
      })
      .catch(r => {
        if (
          r.response &&
          r.response.data &&
          r.response.data.error &&
          r.response.data.error.details &&
          r.response.data.error.details.codes
        ) {
          this.setState({
            errors: { email: r.response.data.error.details.codes },
            isLoading: false
          });
        } else {
          this.setState({
            errors: { email: "Něco se pokazilo, prosím, zkusteto to později." },
            isLoading: false
          });
        }
      });
  };

  isDisabled = () => {
    const { email, emailing } = this.state;

    return (
      !email ||
      (email === this.props.user.email &&
        emailing === this.props.user.emailingAllowed)
    );
  };

  render() {
    const {
      email,
      emailing,
      isLoading,
    } = this.state;
    const { user } = this.props;

    return (
      <Container>
        <Wrapper>
          <H1>OSOBNÍ ÚDAJE</H1>
          <div style={{ marginBottom: "30px" }}>
            <ButtonLikeLink onClick={this.props.openPasswordReset}>
              změnit heslo
            </ButtonLikeLink>
          </div>
          <Form onSubmit={this.onSubmit}>
            <FormGroup>
              <LabelLocal htmlFor="email-field">EMAIL</LabelLocal>
              <Input
                id="email-field"
                type="text"
                name="email"
                value={email}
                onChange={this.onChange}
                invalid={!!this.state.errors.email}
              />
              <FormFeedback>{this.state.errors.email}</FormFeedback>
              {user.emailToBeChanged && (
                <P>
                  {user.emailToBeChanged} - změna emailu čekající na autorizaci.
                  Změnu potvrďte kliknutím na odkaz, který byl zaslán na uvedený
                  email.
                  {/*TODO <a href="">Zrušit změnu</a>*/}
                </P>
              )}
            </FormGroup>
            <FormGroup>
              <Checkbox
                id="emailing-field"
                type="checkbox"
                name="emailing"
                checked={emailing}
                onChange={this.onChange}
              />
              <LabelCheckbox htmlFor="emailing-field">
                Souhlasím s přijímáním občasných emailů od týmu RAMBONATION, s
                tím že můžu své rozhodnutí kdykoliv změnit.
              </LabelCheckbox>
            </FormGroup>
            <ButtonWrapper>
              <Button
                disabled={this.isDisabled()}
                isLoading={isLoading}
                type="submit"
                isButton
              >
                ULOŽIT
              </Button>
            </ButtonWrapper>
          </Form>
        </Wrapper>
      </Container>
    );
  }
}

PersonalDetails.propTypes = {
  user: PropTypes.object.isRequired,
  userToken: PropTypes.string.isRequired,
  openPasswordReset: PropTypes.func.isRequired,
  updateUser: PropTypes.func.isRequired
};

export default PersonalDetails;

const H1 = styled.h1`
  text-transform: uppercase;
  font-family: ${fonts.mainTitle};
  font-size: 30px;
  margin-top: 30px;
  border-bottom: 1px solid white;
`;

const P = styled.p``;

const LabelLocal = styled(Label)`
  font-family: ${fonts.mainTitle};
  margin-bottom: 5px;
`;

const LabelCheckbox = styled(Label)`
  font-family: ${fonts.title};
  margin-bottom: 5px;
  font-size: 12px;
  margin-left: 25px;
`;

const Wrapper = styled.div`
  margin-top: 40px;
  max-width: 400px;
  margin-left: auto;
  margin-right: auto;
`;

const ButtonWrapper = styled.div`
  text-align: center;
`;

const Checkbox = styled(Input)`
  margin-left: 0;
`;
