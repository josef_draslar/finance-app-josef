import React from "react";
import styled from "styled-components";
import fonts from "../utils/fonts";

class HomePage extends React.PureComponent {
  render() {
    return (
      <div>
        <H1>Hlavní strana</H1>
      </div>
    );
  }
}

export default HomePage;

const H1 = styled.h1`
  font-family: ${fonts.mainTitle};
  text-align: center;
  margin-top: 20px;
`;