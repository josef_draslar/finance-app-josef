import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import Join from "../../components/Join";

class ForceUserLogIn extends React.PureComponent {
  state = {
    redirect: false
  };

  render() {
    let { children, user, redirectTo, userToken } = this.props;

    if (this.state.redirect) {
      return <Redirect to={redirectTo} />;
    }

    console.log("user", user, userToken);
    if (!user || !userToken) {
      return (
        <Join
          manualCloseJoin={loggedIn => {
            console.log("here", loggedIn);
            !loggedIn && this.setState({ redirect: true });
          }}
        />
      );
    }

    return children;
  }
}

ForceUserLogIn.propTypes = {
  verifyEmail: PropTypes.bool,
  verifyPayedAccount: PropTypes.bool,
  verifyToken: PropTypes.bool,
  redirectTo: PropTypes.string
};

ForceUserLogIn.defaultProps = {
  verifyEmail: true,
  verifyPayedAccount: true,
  verifyToken: true,
  redirectTo: "/"
};

export default connect(state => ({
  user: state.user.user,
  userToken: state.user.userToken
}))(ForceUserLogIn);
