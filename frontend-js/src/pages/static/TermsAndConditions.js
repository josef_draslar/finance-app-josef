import React from "react";
import { Container } from "reactstrap";
import styled from "styled-components";
import colors from "../../utils/colors";
import fonts from "../../utils/fonts";
import Footer from "../../components/Footer";

export default () => (
      <div>
        <Container>
          <ContentWrapper>
            <H1>Všeobecné obchodní podmínky</H1>
            <div>
              Zaplacením objednávky „Prémiového členství” souhlasí uživatel s
              níže uvedenými podmínkami a zavazuje se dodržovat pravidla v nich
              uvedená.
            </div>
            <H2>I.</H2>
            <H2>Obecná ustanovení</H2>
            <ol>
              <li>
                Poskytovatelem služeb a on-line kurzů (dále jen služba) na
                stránce
                (dále jen „internetová stránka“) je <br />
                IČ: <br />
                zapsán v živnostenském rejstříku<br />
                adresa: a<br />
                kontaktní údaje: <br />
                email: email@email.cz<br />
                telefon: <br />
                www: <br />
                (dále jen „prodávající“, „poskytovatel“)
              </li>
              <li>
                Uživatel je fyzická či právnická osoba, která uhradila
                objednávku on-line kurzu Poskytovatele (dále jen „Uživatel“ /
                „Kupující”).
              </li>
            </ol>
            <H2>II.</H2>
            <H2>Předmět služby</H2>
            <ol>
              <li>
                Předmětem služby je právo Uživatele na přístup k tréninkovým
                video lekcím a tréninkovým plánům uložených na serveru
                Poskytovatele a dalším službám Poskytovatele nabízeným na
                internetové stránce.
              </li>
              <li>
                Ukázková videa slouží jako úvod k nabízené službě a k ověření
                technické dostupnosti pro uživatele. Poskytovatel má právo
                prohlížet si tato ukázková videa pro svoji osobní potřebu
                zdarma.
              </li>
            </ol>
            <H2>III.</H2>
            <H2>Rozsah služby</H2>
            <ol>
              <li>
                Uhrazením ceny služby získává Uživatel právo na přístup k
                objednané službě (dle typu objednávky) poskytovatele za účelem
                svého osobního vzdělávání a rozvoje.
              </li>
            </ol>
            <H2>IV.</H2>
            <H2>Práva a povinnosti Uživatele</H2>
            <ol>
              <li>
                Uživatel je povinen uhradit cenu služby stanovenou na
                internetových stránkách Poskytovatele, a to bezhotovostně (tj.
                buď na základě přímé platby platební kartou, nebo bankovním
                převodem).
              </li>
              <li>
                Uživatel má právo využívat služby objednaného on-line kurzu.
              </li>
              <li>
                Uživatel je povinen otestovat před uhrazením ceny služby, zdali
                mu přehrávání video lekcí, a celkově internetová stránka funguje
                s jeho technickým vybavením (hardware i software) a používaným
                internetovým připojením. K tomuto otestování slouží zdarma
                přístupná vybraná videa a tréninkové plány na internetových
                stránkách Poskytovatele. Úhradou objednávky služby Uživatel
                potvrzuje, že se mu video přehrává a internetová stránka
                zobrazuje bez problémů a akceptuje stejnou kvalitu a způsob
                přehrávání pro samotné prémiové členství. Poskytovatel je
                oprávněn zamítnout pozdější reklamaci Uživatele z tohoto důvodu.
              </li>
              <li>
                Uživatel se zavazuje dodržovat pravidla uvedená v článku 10 –
                Ochrana autorských práv.
              </li>
            </ol>
            <H2>V.</H2>
            <H2>Objednávka</H2>
            <ol>
              <li>
                Kupující prohlašuje, že se seznámil se všemi informacemi
                tykajícími se objednávky, a to na adrese internetové stránky.
                Kupující objednává předmět koupě vyplněním elektronického
                formuláře objednávky prostřednictvím internetové stránky.
                Kupující je povinen objednávku před jejím odesláním zkontrolovat
                a případně opravit. Odeslaná objednávka je právně závazná a
                kupujícímu a prodávajícímu vznikají vzájemná práva a povinnosti,
                tj. prodávající se zavazuje poskytnout kupujícímu předmět koupě,
                službu (dle typu objednávky), včetně všech bonusů a kupující se
                zavazuje uhradit kupní cenu. Kupující odesláním objednávky
                stvrzuje, že se seznámil s obchodními podmínkami pro nákup na
                internetové stránce a že s
                nimi souhlasí. Tyto obchodní podmínky, které jsou zveřejněné na
                internetových stránkách prodávajícího, jsou nedílnou součástí
                kupní smlouvy, která je uzavřena vyplněním a odesláním
                objednávky.
              </li>
            </ol>
            <H2>VI.</H2>
            <H2>Kupní cena, daňový doklad</H2>
            <ol>
              <li>
                V objednávkovém formuláři internetové stránky najdete všechny
                ceny služeb. Uvedené ceny jsou konečné. Prodávající není plátcem
                DPH.
              </li>
              <li>
                Daňový doklad vám bude vystaven na základě uhrazené zálohové
                faktury. Daňový doklad slouží jako doklad o zakoupení on-line
                kurzu.
              </li>
            </ol>
            <H2>VII.</H2>
            <H2>Způsob a forma platby</H2>
            <ol>
              <li>
                Způsob platby<br />
                Platební metody jsou napojeny na platební bránu společnosti
                ComGate, a.s., která poskytuje zabezpečenou technologii
                přijímání platebních karet a online bankovních převodů. Čísla
                platebních karet, kreditních karet a hesla k elektronickému
                bankovnictví zadáváte pomocí zabezpečeného a důvěryhodného
                kanálu společnosti ComGate, a.s.
              </li>
              <li>
                Možnosti plateb<br />
                <ul>
                  <li>
                    Online platební kartou: VISA, VISA electron, MasterCard,
                    Maestro.
                  </li>
                  <li>Bankovním převodem.</li>
                </ul>
              </li>
              <li>
                Forma platby<br />
                <ul>
                  <li>
                    Platba za prémiové členství je prováděna opakovaně (dle
                    zvolené frekvence měsíčně/ročně, které odpovídá cena
                    služby).
                  </li>
                  <li>Platba za ostatní služby je provedena jednorázově.</li>
                </ul>
              </li>
              <li>
                Bonusy<br />
                Všechny bonusy, na které má kupující nárok, budou zpřístupněny
                na internetových stránkách v průběhu zaplaceného prémiového
                členství a to za podmínky, že bude objednávka řádně uhrazena a
                kupující nepožádal v garanční lhůtě o vrácení peněz.
              </li>
            </ol>
            <H2>VIII.</H2>
            <H2>Garance vrácení peněz</H2>
            <ol>
              <li>
                Za své produkty poskytovatel ručí zárukou spokojenosti a garancí
                vrácení peněz.
              </li>
              <li>
                Při objednání online služby má kupující právo odstoupit od
                smlouvy bez udání důvodu, a to nejpozději 14. den ode dne
                objednání služby včetně. Odstoupení je možno provést pouze
                elektronickou formou na e-mailu: email@email.cz s
                prohlášením, že od smlouvy odstupuje a s přiložením kopie
                faktury (daňového dokladu) a čísla účtu, kam chce kupující
                poukázat finanční prostředky. Kupujícímu bude zaslán dobropis s
                částkou odpovídající kupní ceně služby. Částka bude vrácena
                nejpozději do 30 dnů od doručení emailu s odstoupením od
                smlouvy, který bude obsahovat přiložený daňový doklad. Částka
                bude vrácena zpět bankovním převodem. Po odstoupení od smlouvy
                kupujícímu zaniká přístup do členské sekce placeného prémiového
                členství a kupující nebude mít nadále právo využívat prémiové
                členství.
              </li>
            </ol>
            <H2>IX.</H2>
            <H2>Reklamace</H2>
            <ol>
              <li>
                V případě technických problémů s přihlášením do členské sekce
                nebo jiných problémů, které ze strany Provozovatele znemožňují
                užívání produktu, se má uživatel obracet na e-mailový kontakt
                email@email.cz. Požadavky jsou běžně řešeny do 1
                pracovního dne, nejpozději však do 30 dnů od přijetí požadavku.
              </li>
            </ol>
            <H2>X.</H2>
            <H2>Práva a povinnosti Poskytovatele</H2>
            <ol>
              <li>
                Poskytovatel má povinnost nastavit Uživateli přístupová práva k
                objednané službě do 3 pracovních dnů po obdržení celé úhrady
                kurzu od Uživatele na svůj účet. Uživatel souhlasí s tím, že
                Poskytovatel zahájí poskytování služeb dle těchto podmínek v
                souladu s předchozí větou, tj. před uplynutím zákonné lhůty pro
                odstoupení od smlouvy, a Uživatel proto nebude mít právo
                odstoupit od smlouvy dle příslušných ustanovení zákona č.
                89/2012 Sb., občanského zákoníku.
              </li>
              <li>
                Poskytovatel má právo předčasně ukončit bez náhrady přístup
                Uživatele k online kurzu za předpokladu, že Uživatel bude
                porušovat článek 6 těchto podmínek.
              </li>
              <li>
                Poskytovatel může přiměřeně informovat formou reklamních sdělení
                Uživatele o svých dalších službách a to s využitím e-mailové
                adresy Uživatele, přičemž Uživatel s tímto vysloveně souhlasí
                při Registraci nebo v uživatelském profilu zaškrtnutím
                příslušného pole. Poskytovatel je povinen přestat zasílat
                případná reklamní sdělení Uživateli v případě, že mu toto
                Uživatel sdělí e-mailovou zprávou.
              </li>
              <li>
                Uživatel nemá v případě výpadku serveru žádný nárok na finanční
                kompenzaci nebo vrácení zaplaceného poplatku. Poskytovatel je
                povinen vynaložit nezbytné úsilí na odstranění technických
                problémů serveru.
              </li>
            </ol>
            <H2>XI.</H2>
            <H2>Ochrana autorských práv</H2>
            <ol>
              <li>
                Služby poskytované na internetové stránce jsou autorským dílem
                Poskytovatele a ten neposkytuje Uživateli právo šířit dílo dále
                jakýmikoliv cestami elektronické, či jiné komunikace nebo je
                jakýmkoli jiným způsobem zveřejňovat nebo poskytovat třetím
                osobám.
              </li>
              <li>
                Uživatel se zavazuje nestahovat videa ani další obsah prémiového
                členství na internetovou stránce na svůj pevný disk či jiné
                off-line či online médium určené pro ukládání digitálních dat.
                Stejně tak se Uživatel zavazuje nepořizovat žádné audio-video
                záznamy přehrávaného videa ani dalšího obsahu v rámci prémiového
                členství. Veškeré tyto a další činnosti podobného charakteru
                jsou v rozporu s přáním provozovatele a těmito smluvními
                podmínkami.
              </li>
              <li>
                Uživatel se zavazuje neposkytovat třetím osobám internetovou
                adresu, na které jsou videa služby umístěna.
              </li>
              <li>
                Uživatel se zavazuje chránit své přístupové údaje (email a
                heslo), prostřednictvým kterých se provádí autorizace přístupu k
                prémiovému členství. Uživatel nesmí umožnit třetím osobám, aby
                se přihlašovaly pod jeho přístupovými údaji. V případě, že
                Uživatel třetí osobě takový přístup umožní, nese plnou
                odpovědnost za případný vznik škody jako by se zneužití dopustil
                sám.
              </li>
            </ol>
            <H2>XII.</H2>
            <H2>Závěrečná ustanovení</H2>
            <ol>
              <li>
                Ve smyslu zákona č. 101/2000 Sb. o ochraně osobních údajů
                Uživatel souhlasí s tím, že Poskytovatel uchovává ve své
                databázi údaje vložené Uživatelem při používání internetové
                stránky, a že je oprávněn tyto údaje zpracovávat pro účely
                obchodních vztahů s Uživatelem a neposkytovat je třetím stranám.
                Tento souhlas se uděluje po celou dobu trvání obchodního vztahu
                s Poskytovatelem a po celou dobu používání internetové stránky,
                nestanoví-li zvláštní zákon lhůtu delší. Uživatel bere na
                vědomí, že má práva zakotvená v § 12 a § 21 zákona o ochraně
                osobních údajů.
              </li>
              <li>
                Jsou-li nebo budou-li jednotlivá ustanovení těchto smluvních
                podmínek neplatné, zůstávají ostatní ustanovení v platnosti.
                Takto vzniklá mezera bude nahrazena úpravou odpovídající účelu a
                smyslu těchto podmínek.
              </li>
              <li>
                Uživatel rozumí tomu, že všechny informace poskytované v rámci
                prémiového členství Poskytovatele jsou určeny výhradně ke
                studijním účelům tématu tréninku a slouží jako všeobecná
                doporučení bez znalosti konkrétní situace jednotlivého
                Uživatele. Poskytovatel neodpovídá za způsob, jakým Uživatelé
                jeho rady aplikují v praxi, a proto nepřebírá odpovědnost za
                konkrétní rozhodnutí jednotlivých uživatelů a jejich případné
                následky. Přístup k tréninkům a stravě je odpovědností každého
                Uživatele a jedině on sám nese za svá rozhodnutí plnou
                odpovědnost. Ustanovení 2950 zákona č. 89/2012 Sb., občansky
                zákoník, proto na právní vztahy Poskytovatele a Uživatele
                nedopadá.
              </li>
              <li>
                Tyto Podmínky a vztah mezi Poskytovatelem a Uživatelem se řídí
                právním řádem České republiky.
              </li>
              <li>
                Jakákoli komunikace mezi Poskytovatelem a Uživatelem probíhá
                elektronicky formou elektronické pošty (emailu).
              </li>
              <li>
                Tyto podmínky užívání nabývají účinnosti dne 1. 1. 2019.
                Poskytovatel si vyhrazuje právo tyto podmínky užívání kdykoliv
                změnit, o čemž bude Uživatel vyrozuměn nejméně s měsíčním
                předstihem před nabytím změny účinnosti podmínek užívání a bude
                mu dána možnost smlouvu s Poskytovatelem z tohoto důvodu
                vypovědět.
              </li>
            </ol>
          </ContentWrapper>
        </Container>
        <Footer />
      </div>
    );

const ContentWrapper = styled.div`
  color: ${colors.dark};
`;

const H1 = styled.h1`
  margin-top: 35px;
  text-align: center;
  font-family: ${fonts.mainTitle};
`;

const H2 = styled.h2`
  font-size: 18px;
  text-transform: uppercase;
  text-align: center;
  text-decoration: none;
`;
