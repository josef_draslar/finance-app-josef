import React from "react";
import { Container } from "reactstrap";
import styled from "styled-components";
import colors from "../../utils/colors";
import fonts from "../../utils/fonts";
import Footer from "../../components/Footer";

export default () => (
      <div>
        <Container>
          <ContentWrapper>
            <H1>Podmínky ochrany osobních údajů</H1>
            <H2>I.</H2>
            <H2>Základní ustanovení</H2>
            <ol>
              <li>
                Správcem osobních údajů podle čl. 4 bod 7 nařízení Evropského
                parlamentu a Rady (EU) 2016/679 o ochraně fyzických osob v
                souvislosti se zpracováním osobních údajů a o volném pohybu
                těchto údajů (dále jen: „GDPR”) je . IČ  se
                sídlem . (dále jen: „správce“).
              </li>
              <li>
                Kontaktní údaje správce jsou<br />
                adresa: <br />
                email: email@email.cz<br />
                telefon: <br />
              </li>
              <li>
                Osobními údaji se rozumí veškeré informace o identifikované nebo
                identifikovatelné fyzické osobě; identifikovatelnou fyzickou
                osobou je fyzická osoba, kterou lze přímo či nepřímo
                identifikovat, zejména odkazem na určitý identifikátor,
                například jméno, identifikační číslo, lokační údaje, síťový
                identifikátor nebo na jeden či více zvláštních prvků fyzické,
                fyziologické, genetické, psychické, ekonomické, kulturní nebo
                společenské identity této fyzické osoby.
              </li>
              <li>Správce nejmenoval pověřence pro ochranu osobních údajů.</li>
            </ol>
            <H2>II.</H2>
            <H2>Zdroje a kategorie zpracovávaných osobních údajů</H2>
            <ol>
              <li>
                Správce zpracovává osobní údaje, které jste mu poskytl/a nebo
                osobní údaje, které správce získal na základě plnění Vaší
                objednávky.
              </li>
              <li>
                Správce zpracovává Vaše identifikační a kontaktní údaje a údaje
                nezbytné pro poskytování služby či objednávky.
              </li>
            </ol>
            <H2>III.</H2>
            <H2>Zákonný důvod a účel zpracování osobních údajů</H2>
            <ol>
              <li>
                Zákonným důvodem zpracování osobních údajů je
                <ul>
                  <li>
                    plnění smlouvy mezi Vámi a správcem podle čl. 6 odst. 1
                    písm. b) GDPR,
                  </li>
                  <li>
                    oprávněný zájem správce na poskytování přímého marketingu
                    (zejména pro zasílání obchodních sdělení a newsletterů)
                    podle čl. 6 odst. 1 písm. f) GDPR,
                  </li>
                  <li>
                    Váš souhlas se zpracováním pro účely poskytování přímého
                    marketingu (zejména pro zasílání obchodních sdělení a
                    newsletterů) podle čl. 6 odst. 1 písm. a) GDPR ve spojení s
                    § 7 odst. 2 zákona č. 480/2004 Sb., o některých službách
                    informační společnosti v případě, že nedošlo k objednávce
                    zboží nebo služby.
                  </li>
                </ul>
              </li>
              <li>
                Účelem zpracování osobních údajů je
                <ul>
                  <li>
                    vyřízení Vaší objednávky a výkon práv a povinností
                    vyplývajících ze smluvního vztahu mezi Vámi a správcem; při
                    objednávce jsou vyžadovány osobní údaje, které jsou nutné
                    pro úspěšné vyřízení objednávky (jméno a adresa, kontakt),
                    poskytnutí osobních údajů je nutným požadavkem pro uzavření
                    a plnění smlouvy, bez poskytnutí osobních údajů není možné
                    smlouvu uzavřít či jí ze strany správce plnit,
                  </li>
                  <li>
                    zasílání obchodních sdělení a činění dalších marketingových
                    aktivit.{" "}
                  </li>
                </ul>
              </li>
              <li>
                Ze strany správce nedochází k automatickému individuálnímu
                rozhodování ve smyslu čl. 22 GDPR.
              </li>
            </ol>
            <H2>IV.</H2>
            <H2>Doba uchovávání údajů</H2>
            <ol>
              <li>
                Správce uchovává osobní údaje
                <ul>
                  <li>
                    po dobu nezbytnou k výkonu práv a povinností vyplývajících
                    ze smluvního vztahu mezi Vámi a správcem a uplatňování
                    nároků z těchto smluvních vztahů (po dobu 15 let od ukončení
                    smluvního vztahu).
                  </li>
                  <li>
                    po dobu, než je odvolán souhlas se zpracováním osobních
                    údajů pro účely marketingu, nejdéle …. let, jsou-li osobní
                    údaje zpracovávány na základě souhlasu.
                  </li>
                </ul>
              </li>
              <li>
                Po uplynutí doby uchovávání osobních údajů správce osobní údaje
                vymaže.
              </li>
            </ol>
            <H2>V.</H2>
            <H2>Příjemci osobních údajů (subdodavatelé správce)</H2>
            <ol>
              <li>
                Příjemci osobních údajů jsou osoby
                <ul>
                  <li>
                    podílející se na dodání zboží / služeb / realizaci plateb na
                    základě smlouvy.
                  </li>
                </ul>
              </li>
              <li>
                Správce nemá v úmyslu předat osobní údaje do třetí země (do země
                mimo EU) nebo mezinárodní organizaci.
              </li>
            </ol>
            <H2>VI.</H2>
            <H2>Vaše práva</H2>
            <ol>
              <li>
                Za podmínek stanovených v GDPR máte
                <ul>
                  <li>
                    právo na přístup ke svým osobním údajům dle čl. 15 GDPR,
                  </li>
                  <li>
                    právo opravu osobních údajů dle čl. 16 GDPR, popřípadě
                    omezení zpracování dle čl. 18 GDPR.
                  </li>
                  <li>právo na výmaz osobních údajů dle čl. 17 GDPR.</li>
                  <li>
                    právo vznést námitku proti zpracování dle čl. 21 GDPR a
                  </li>
                  <li>
                    právo odvolat souhlas se zpracováním písemně nebo
                    elektronicky na adresu nebo email správce uvedený v čl. III
                    těchto podmínek.
                  </li>
                </ul>
              </li>
              <li>
                Dále máte právo podat stížnost u Úřadu pro ochranu osobních
                údajů v případě, že se domníváte, že bylo porušeno Vaší právo na
                ochranu osobních údajů.
              </li>
            </ol>
            <H2>VII.</H2>
            <H2>Podmínky zabezpečení osobních údajů</H2>
            <ol>
              <li>
                Správce prohlašuje, že přijal veškerá vhodná technická a
                organizační opatření k zabezpečení osobních údajů.
              </li>
              <li>
                Správce přijal potřebná technická opatření k zabezpečení
                datových úložišť a úložišť osobních údajů v listinné podobě.
              </li>
              <li>
                Správce prohlašuje, že k osobním údajům mají přístup pouze jím
                pověřené osoby.
              </li>
            </ol>
            <H2>VIII.</H2>
            <H2>Závěrečná ustanovení</H2>
            <ol>
              <li>
                Odesláním objednávky z internetového objednávkového formuláře,
                registrací, či vyplněním osoboních údajů v profilu potvrzujete,
                že jste seznámen/a s podmínkami ochrany osobních údajů a že je v
                celém rozsahu přijímáte.
              </li>
              <li>
                Správce je oprávněn tyto podmínky změnit. Novou verzi podmínek
                ochrany osobních údajů zveřejní na svých internetových stránkách
                a zároveň Vám zašle novou verzi těchto podmínek Vaši e-mailovou
                adresu, kterou jste správci poskytl/a.
              </li>
            </ol>
            Tyto podmínky nabývají účinnosti dnem 1.1.2019.
          </ContentWrapper>
        </Container>
        <Footer />
      </div>
    );

const ContentWrapper = styled.div`
  color: ${colors.dark};
`;

const H1 = styled.h1`
  margin-top: 35px;
  text-align: center;
  font-family: ${fonts.mainTitle};
`;

const H2 = styled.h2`
  font-size: 18px;
  text-transform: uppercase;
  text-align: center;
  text-decoration: none;
`;
