import React from "react";
import styled from "styled-components";
import fonts from "../utils/fonts";

class HistoryPage extends React.PureComponent {
  render() {
    return (
      <div>
        <H1>Historie</H1>
      </div>
    );
  }
}

export default HistoryPage;

const H1 = styled.h1`
  font-family: ${fonts.mainTitle};
  text-align: center;
  margin-top: 20px;
`;
