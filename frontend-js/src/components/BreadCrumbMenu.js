import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import { media } from "../utils/Media";
import styled from "styled-components";
import colors from "../utils/colors";
import fonts from "../utils/fonts";

class BreadCrumbMenu extends React.PureComponent {
  render() {
    const { links } = this.props;
    return (
      <BreadCrumb>
        <Ul>
          {links.map(({ to, label }, key) => (
            <Li key={key} className={key === 0 ? "active" : ""}>
              <LinkStyled
                to={to ? to : "/"}
                className={!to || key === 0 ? "disabled-link" : ""}
              >
                {label || <i className="fa fa-home" />}
              </LinkStyled>
            </Li>
          ))}
        </Ul>
      </BreadCrumb>
    );
  }
}

BreadCrumbMenu.propTypes = {
  links: PropTypes.arrayOf(PropTypes.object).isRequired
};

export default BreadCrumbMenu;

const LinkStyled = styled(Link)`
  display: inline-block;
  font-size: 0.9em;
  font-weight: 600;
  padding: 10px 30px 10px 45px; /* Adjusting padding to get the proper space */
  color: ${colors.lighterGray};
  text-decoration: none;
  text-transform: uppercase;
  transform: skew(20deg);

  &:focus,
  &:hover {
    text-decoration: none;
    color: ${colors.white};
  }

  ${media.lessThan("sm")`
      font-family: ${fonts.mainTitle};
      padding: 10px 10px 10px 30px;
    `};
`;

const BreadCrumb = styled.div`
  margin-top: 25px;
  margin-bottom: 10px;
  display: inline-block; /* The menu will have width as needed, not 100% */
  position: relative;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
  font-size: 16px;
  line-height: 18px;
  border-radius: 2px; /* Little curvature in the borders */
  font-family: ${fonts.mainTitle};

  ${media.lessThan("sm")`
    font-size: 14px;
  `};
`;

const Li = styled.li`
  background-color: ${colors.middleDark};
  border-right: 1px ${colors.lighterGray} solid;
  margin-left: -20px !important; /* Pull the items to the left, so the rounded right side will get over them */
  position: relative;

  transform: skew(-20deg);
`;

const Ul = styled.ul`
  display: flex; /* A key part of our menu, displays items side by side, and allows reversing them */
  flex-direction: row-reverse; /* Reverse the items */
  /* Reset styles, overwrite browser defaults */
  list-style: none;
  margin: 0;
  padding: 0;

  & li {
    margin: 0;
  }

  & i {
    transform: scale(1.4);
  }

  & li:hover,
  & li:focus {
    background-color: ${colors.light};
  }

  /* Remove the shadow for first item, last in the right side when it gets reversed */
  & li:first-child {
    box-shadow: none;
  }

  /* Active item is a bit different */
  & li.active ${LinkStyled} {
    color: ${colors.middleDark};
  }

  & li.active {
    background-color: ${colors.lighterGray};
  }

  & li.active::after {
    border-left-color: ${colors.lighterGray};
  }

  /* Remove the shadow for the active's next item, to match the design more accurately */
  & li.active + li {
    box-shadow: none;
  }

  & li.active:hover a {
    cursor: default;
  }
`;
