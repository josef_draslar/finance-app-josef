import styled from "styled-components";

export const ButtonLikeLink = styled.button`
  background: none!important;
  border: none;
  padding: 0!important;
  color: #069;
  text-decoration: underline;
  cursor: pointer;
`;