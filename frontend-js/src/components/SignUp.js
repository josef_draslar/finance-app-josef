import React from "react";
import axios from "../utils/Axios";
import styled from "styled-components";
import { Alert, Form, FormFeedback, FormGroup, Input } from "reactstrap";
import PropTypes from "prop-types";
import Button from "./Button";
import { pushNotificationMessageIfPossible } from "./NotificationMessage";
import {
  ButtonWrapper,
  Checkbox,
  LabelCheckbox,
  LabelLocal,
  Or,
  SmallText,
  SmallTextSpan,
  Text,
  EmailWrapper,
  FacebookButtonWrapper,
  A,
  ModalHeader
} from "./ModalJoinElements";
import colors from "../utils/colors";
import fonts from "../utils/fonts";
import { ButtonLikeLink } from "./BasicElements";

class SignUp extends React.PureComponent {
  state = {
    email: "",
    password: "",
    emailing: false,
    isLoading: false,
    errors: {}
  };

  onChange = e => {
    const { name, value, type } = e.target;

    this.setState({
      [name]: type === "checkbox" ? !this.state[name] : value,
      errors: {
        ...this.state.errors,
        [name]: null
      },
      info: ""
    });
  };

  storeAdditionalData = () =>
    localStorage.setItem(
      "additionalTourRegistrationData",
      JSON.stringify(this.props.additionalUserData)
    );

  onSubmit = e => {
    e.preventDefault();
    const { email, password, emailing } = this.state;

    const referrer = localStorage.getItem("referrer");
    localStorage.removeItem("referrer");
    const { close } = this.props;
    this.setState({ isLoading: true });
    axios
      .post("users", {
        email: email.toLowerCase(),
        password,
        emailing,
        referrer: referrer ? referrer : undefined
      })
      .then(({ data }) => {
        window.akce = false;
        this.setState({ isLoading: false });
        close();
        pushNotificationMessageIfPossible({
          title: "Registrace úspěšně dokončena",
          message: localStorage.getItem("registerToEvent")
            ? "Už se jen přihlásit a registrace na akci bude dokončena. Zkontrolujte svůj email a klikněte a verifikační odkaz, pro ověření registrace, před tím než se přihlásíte."
            : "Zkontrolujte svůj email a klikněte a verifikační odkaz, pro ověření registrace, před tím než se přihlásíte. (Nezapomeňte zkontrolovat složku Spam.)",
          type: "SUCCESS"
        });
      })
      .catch(({ response }) => {
        this.setState({
          errors: {
            ...this.state.errors,
            generic:
              response &&
              response.data &&
              response.data.errors &&
              response.data.errors.length > 0
                ? response.data.errors[0].message
                : "Něco se pokazilo, prosím zkuste to později."
          },
          isLoading: false
        });
      });
  };

  isDisabled = () => {
    const { email, password } = this.state;

    return !(email && password && password.length > 5);
  };

  swapLocal = () => {
    if (this.props.inTour) {
      this.storeAdditionalData();
    }
    this.props.swap();
  };

  render() {
    const { email, password, emailing, isLoading, errors } = this.state;
    const { inTour, swap } = this.props;

    return (
      <Wrapper inTour={inTour}>
        {!inTour &&
          <ModalHeader toggle={this.props.close}>Registrace</ModalHeader>}
        <div className="row">
          <div className="col-sm-6">
            <H6>Pomocí sociálních sítí</H6>
            <FacebookButtonWrapper>
              <Button
                href="/auth/facebook"
                onClick={this.storeAdditionalData}
                iconKeyFull="fab fa-facebook-f"
                backgroundColor={colors.facebookBlue}
              >
                Facebook
              </Button>
            </FacebookButtonWrapper>
          </div>
          <div className="col-sm-6">
            <EmailWrapper inTour={inTour}>
              <Or inTour={inTour}>nebo</Or>
              <H6>Pomocí emailu</H6>
              <Form onSubmit={this.onSubmit}>
                <FormGroup>
                  <LabelLocal htmlFor="email-field">EMAIL</LabelLocal>
                  <Input
                    id="email-field"
                    type="text"
                    name="email"
                    value={email}
                    onChange={this.onChange}
                    invalid={!!this.state.errors.email}
                  />
                  <FormFeedbackStyled inTour={inTour}>
                    Email je již zabraný, vyberte prosím jiný , nebo se{" "}
                    <ButtonLikeLink onClick={this.swapLocal}>
                      přihlašte
                    </ButtonLikeLink>.
                  </FormFeedbackStyled>
                </FormGroup>
                <FormGroup>
                  <LabelLocal htmlFor="password-field">HESLO</LabelLocal>
                  <Input
                    id="password-field"
                    type="password"
                    name="password"
                    value={password}
                    onChange={this.onChange}
                  />
                </FormGroup>
                <FormGroup>
                  <Checkbox
                    id="emailing-field"
                    type="checkbox"
                    name="emailing"
                    value={emailing}
                    onChange={this.onChange}
                  />
                  <LabelCheckbox htmlFor="emailing-field">
                    Souhlasím s přijímáním občasných emailů od týmu RAMBONATION,
                    s tím že můžu své rozhodnutí kdykoliv změnit.
                  </LabelCheckbox>
                </FormGroup>
                {errors.generic &&
                  <Alert color="danger">
                    {errors.generic}
                  </Alert>}
                <ButtonWrapper>
                  <Button
                    disabled={this.isDisabled()}
                    isLoading={isLoading}
                    type="submit"
                    isButton
                    inverseColors={!!inTour}
                  >
                    Registruj se
                  </Button>
                </ButtonWrapper>
              </Form>
            </EmailWrapper>
          </div>
        </div>

        {!!swap &&
          <Text>
            Už máš účet? <A onClick={this.swapLocal}>Přihlaš se</A>
          </Text>}
        <SmallText>
          <SmallTextSpan>
            Registrací souhlasím s:{" "}
            <A
              href="/vseobecne-obchodni-podminky"
              target="_blank"
            >
              Podmínky použítí
            </A>{" "}
            a{" "}
            <A href="/podminky-osobnich-udaju" target="_blank">
              Zásady ochrany osobních údajů
            </A>
          </SmallTextSpan>
        </SmallText>
      </Wrapper>
    );
  }
}

SignUp.propTypes = {
  close: PropTypes.func.isRequired,
  swap: PropTypes.func,
  inTour: PropTypes.bool,
  additionalUserData: PropTypes.object
};

SignUp.defaultProps = {
  additionalUserData: {}
};

export default SignUp;

const FormFeedbackStyled = styled(FormFeedback)`
  ${props =>
    !!props.inTour &&
    `background-color: white; border-radius: 2px; padding: 2px;`};
`;

const Wrapper = styled.div`${props => !!props.inTour && `font-size: 18px;`};`;

export const H6 = styled.h6`
  text-align: center;
  font-size: inherit;
  font-family: ${fonts.title};
`;
