import React from "react";
import PropTypes from "prop-types";
import axios from "../utils/Axios";
import { Form, FormFeedback, FormGroup, Input } from "reactstrap";
import Button from "./Button";
import {
    ButtonWrapper,
    H6,
    ModalHeader,
    LabelLocal,
    Or,
    Text,
    EmailWrapper,
    FacebookButtonWrapper,
     AOnClick
} from "./ModalJoinElements";
import colors from "../utils/colors";

class LogIn extends React.PureComponent {
  state = {
    email: "",
    password: "",
    isLoading: false,
    errors: {}
  };

  onChange = e => {
    const { name, value } = e.target;

    this.setState({
      [name]: value,
      errors: {
        ...this.state.errors,
        [name]: null
      }
    });
  };

  onSubmit = e => {
    e.preventDefault();
    const { email, password } = this.state;
    const { success, close } = this.props;
    this.setState({ isLoading: true });
    axios
      .post("users/login", {
        email: email.toLowerCase(),
        password
      })
      .then(({ data }) => {
          this.setState({ isLoading: false });
          success(data);

          close(true);
      })
      .catch(({ response }) => {
        console.log(response, response.data);
        this.setState({
          errors: {
            ...this.state.errors,
            email: "Kombinace emailu a hesla není správná."
          },
          isLoading: false
        });
      });
  };

  isDisabled = () => {
    const { email, password } = this.state;

    return !(email && password && password.length > 5);
  };

  facebookIsLoading = () => this.setState({ facebookIsLoading: true });

  render() {
    const { email, password, isLoading } = this.state;
    const { swap, openPasswordReset } = this.props;
    return (
      <div>
        <ModalHeader toggle={this.props.close}>Přihlášení</ModalHeader>
        <div className="row">
          <div className="col-sm-6">
            <H6>Pomocí sociálních sítí</H6>
            <FacebookButtonWrapper>
              <Button
                href="/auth/facebook"
                iconKeyFull="fab fa-facebook-f"
                backgroundColor={colors.facebookBlue}
                isLoading={this.state.facebookIsLoading}
                onClick={this.facebookIsLoading}
              >
                Facebook
              </Button>
            </FacebookButtonWrapper>
          </div>
          <div className="col-sm-6">
            <EmailWrapper>
              <Or>nebo</Or>
              <H6>Pomocí emailu</H6>
              <Form onSubmit={this.onSubmit}>
                <FormGroup>
                  <LabelLocal htmlFor="email-field">EMAIL</LabelLocal>
                  <Input
                    id="email-field"
                    type="text"
                    name="email"
                    value={email}
                    onChange={this.onChange}
                    invalid={!!this.state.errors.email}
                  />
                  <FormFeedback>{this.state.errors.email}</FormFeedback>
                </FormGroup>
                <FormGroup>
                  <LabelLocal htmlFor="password-field">HESLO</LabelLocal>
                  <Input
                    id="password-field"
                    type="password"
                    name="password"
                    value={password}
                    onChange={this.onChange}
                  />
                  <FormFeedback>Zadené heslo nesouhlasí.</FormFeedback>
                </FormGroup>
                <ButtonWrapper>
                  <Button
                    disabled={this.isDisabled()}
                    isLoading={isLoading}
                    type="submit"
                    isButton
                  >
                    Přihlaš se
                  </Button>
                </ButtonWrapper>
              </Form>
            </EmailWrapper>
          </div>
        </div>

        <Text>
          Ještě nemáš účet?{" "}
          <AOnClick onClick={swap}>
            Registruj se
          </AOnClick>
        </Text>
        <Text>
          <AOnClick onClick={openPasswordReset}>
            Resetuj heslo
          </AOnClick>
        </Text>
      </div>
    );
  }
}

LogIn.propTypes = {
  close: PropTypes.func.isRequired,
  swap: PropTypes.func.isRequired,
  success: PropTypes.func.isRequired,
  openPasswordReset: PropTypes.func.isRequired
};

export default LogIn;
