import React from "react";
import styled from "styled-components";
import colors from "../utils/colors";
import { Container, Col, Row } from "reactstrap";

export default () => (
    <Wrapper>
      <Container>
        <Row>
          <Col>
            <SocialsIconLinkAuthor
              target="_blank"
              rel="noopener"
              href="https://www.facebook.com/RambonationOfficial/"
              color={colors.lightHover}
            >
              <i
                className="fab fa-facebook-square"
                title="Facebook Rambonation Official"
              />
            </SocialsIconLinkAuthor>
            <SocialsIconLinkAuthor
              target="_blank"
              rel="noopener"
              href="https://www.instagram.com/rambonation_official/"
              color={colors.lightHover}
            >
              <i className="fab fa-instagram" title="Instagram Rambo Nation" />
            </SocialsIconLinkAuthor>
            <SocialsIconLinkAuthor
              target="_blank"
              rel="noopener"
              href="https://www.youtube.com/channel/UCGLkw5FtXZ7Vy2yDeeerDQQ"
              color={colors.lightHover}
            >
              <i className="fab fa-youtube" title="Youtube Rambo Nation" />
            </SocialsIconLinkAuthor>
          </Col>
        </Row>
        <Div>
          <MailTo href="mailto:email@email.cz">
            info@rambonation.com
          </MailTo>
        </Div>
        <Row>
          <Col style={{ marginTop: "20px" }}>
            <span style={{ color: "rgb(100,100,100)" }}>
              2019 © RamboNation.com
            </span>
          </Col>
        </Row>
      </Container>
    </Wrapper>
  );

const Wrapper = styled.div`
  margin-top: 70px;
  border-top: rgb(50, 50, 50) solid 1px;
  padding-top: 25px;
  text-align: center;
  background-color: ${colors.white};
`;

const SocialsIconLink = styled.a`
  display: inline-block;
  margin: 8px;
  font-size: 38px;
  color: ${colors.dark};

  &:hover,
  &:focus {
    text-decoration: none;
    color: ${props => props.color} !important;
    cursor: pointer;
  }
`;

const SocialsIconLinkAuthor = styled(SocialsIconLink)`
  color: ${colors.light};
  font-size: 30px;
`;

const Div = styled.div`
  color: ${colors.dark};
  font-size: 20px;
`;

const MailTo = styled.a`
  color: ${colors.light};
  &:hover,
  &:focus {
    text-decoration: none;
    color: ${colors.lightHover};
  }
`;
