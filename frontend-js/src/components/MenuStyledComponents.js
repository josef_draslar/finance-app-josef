import React from "react";
import { NavItem, NavLink } from "reactstrap";
import styled, { css } from "styled-components";
import PropTypes from "prop-types";

import colors from "../utils/colors";
import { media } from "../utils/Media";
import fonts from "../utils/fonts";
import {withTranslation} from "../utils/multiLanguageSupport";
import {Link} from "react-router-dom";

class NavItemInner extends React.PureComponent {
    render() {
        const {onClick, name, isPrimary, url, hideForMobile, isMobile, isInlined, t} = this.props;
        if(url) {
            return (
            <NavItemMainLocal hideForMobile={hideForMobile} isPrimary={isPrimary} isInlined={isInlined}>
                <NavLinkStyled
                    isPrimary={isPrimary}
                    tag={Link}
                    to={url}
                    className={url === window.location.pathname ? "isActive" : ""}
                    onClick={onClick}
                    isMobile={isMobile}
                >
                    {t("NavBar." + name)}
                </NavLinkStyled>
            </NavItemMainLocal>
            )
        } else {
            return (
                <NavItemMainLocal isPrimary={isPrimary} isInlined={isInlined}>
                    <NavAStyled
                        isPrimary={isPrimary}
                        onClick={onClick}
                        isMobile={isMobile}
                    >
                        {t("NavBar." + name)}
                    </NavAStyled>
                </NavItemMainLocal>
            )
        }
    }
}

NavItemInner.propTypes = {
    name: PropTypes.string.isRequired,
    onClick: PropTypes.func,
    url: PropTypes.string,
    isPrimary: PropTypes.bool,
    hideForMobile: PropTypes.bool,
    isMobile: PropTypes.bool,
    isInlined: PropTypes.bool
};

export const NavigationItem = withTranslation(NavItemInner);

const navLinkBase = css`
    background: none;
    border: none;
    position: relative;
    font-size: 17px;
    line-height: 22px;
    font-weight: 600;
    color: ${colors.dark};
    font-family: ${fonts.mainTitle};
    letter-spacing: .1em;
    text-transform: uppercase;
    padding: 10px;
    padding-bottom: 8px;
    
    -webkit-transition: color .2s ease-out;
    -moz-transition: color .2s ease-out;
    transition: color .2s ease-out;
    
    ${props =>
      !props.isMobile &&
      `
        top: 50%;
        transform: translateY(-50%);
    `}
    
    &:hover,
    &:focus,
    &.isActive {
        text-decoration: none;
        color: ${colors.light};
    }
    
    &:hover,
    &:focus{
        cursor: pointer;
    }
    
  ${props =>
    props.isPrimary &&
    css`
      border: 1px solid ${colors.light};
      color: ${colors.light};

      &.isActive,
      &:hover,
      &:focus {
        color: ${colors.white};
        background-color: ${colors.lightHover};
      }
    `};
`;

export const NavLinkStyled = styled(({isPrimary, isMobile, ...rest})=><NavLink {...rest}/>)`
  ${navLinkBase}
`;

export const NavAStyled = styled.button`${navLinkBase};`;

export const NavItemMainLocal = styled(({ isPrimary, hideForMobile, isInlined, ...rest }) =>
  <NavItem {...rest} />
)`
  position: relative;

  height: 100%;

  ${props =>
    props.hideForMobile &&
    css`
      ${media.lessThan("sm")`
          display: none !important;
      `};
    `};

  ${props =>
    props.isPrimary &&
    css`
      padding-left: 20px;
      padding-right: 20px;
    `};

  ${props =>
    props.isInlined &&
    css`
      display: inline-block;
    `};
`;

export const NavItemMobileLocal = styled(NavItem)`
  color: ${colors.light};

  & > a {
    position: relative;
    width: auto;
    display: inline-block;
    height: 100%;
    line-height: 50px;
    padding: 0 0.5em 0 24px;
    padding-left: 24px !important;
  }

  & > a.isActive {
    color: ${colors.dark} !important;
  }

  & > a.isActive:after {
    content: "";
    position: absolute;
    top: 0;
    left: 0;
    display: block;
    width: 9px;
    height: 100%;
    background-color: ${colors.light};
  }
  
  display: inline;
  width: auto;
  float: left;
`;