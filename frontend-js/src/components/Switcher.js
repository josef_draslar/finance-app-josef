import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import colors from "../utils/colors";
import fonts from "../utils/fonts";

class Switcher extends React.PureComponent {
  render() {
    const {
      optionsObject,
      options,
      chosen,
      choose,
      disableNoPremium
    } = this.props;

    if (optionsObject)
      return (
        <Wrapper>
          <Ul>
            {optionsObject.map(({ name, disabled }, key) => (
              <Li isChosen={chosen === key} key={key}>
                <A
                  onClick={() => choose(key)}
                  isChosen={chosen === key}
                  disabled={disableNoPremium.indexOf(key) !== -1 || disabled}
                >
                  {name}
                  {disableNoPremium.indexOf(key) !== -1 ? (
                    <ActivatePremium>AKTIVUJ PRÉMIUM</ActivatePremium>
                  ) : disabled ? (
                    <ActivatePremium commingSoon>JIŽ BRZY</ActivatePremium>
                  ) : (
                    ""
                  )}
                </A>
              </Li>
            ))}
          </Ul>
        </Wrapper>
      );

    return (
      <Wrapper>
        <Ul>
          {options.map((option, key) => (
            <Li isChosen={chosen === key} key={key}>
              <A
                onClick={() => choose(key)}
                isChosen={chosen === key}
                disabled={disableNoPremium.indexOf(key) !== -1}
              >
                {option}
                {disableNoPremium.indexOf(key) !== -1 ? (
                  <ActivatePremium>AKTIVUJ PRÉMIUM</ActivatePremium>
                ) : (
                  ""
                )}
              </A>
            </Li>
          ))}
        </Ul>
      </Wrapper>
    );
  }
}

Switcher.propTypes = {
  chosen: PropTypes.number.isRequired,
  options: PropTypes.arrayOf(PropTypes.string).isRequired,
  optionsObject: PropTypes.arrayOf(PropTypes.object).isRequired,
  choose: PropTypes.func.isRequired,
  disableNoPremium: PropTypes.arrayOf(PropTypes.number)
};

Switcher.defaultProps = {
  disableNoPremium: []
};

export default Switcher;

const Wrapper = styled.div`
  text-align: center;
`;

const Ul = styled.ul`
  vertical-align: middle;
  margin: 0;
  padding: 0;
  list-style: none;
  position: relative;
  display: inline-block;
  margin-bottom: 30px;
`;

const Li = styled.li`
  padding: 0 16px;
  vertical-align: middle;
  margin: 0 0 10px;
  padding: 0 10px;
  cursor: pointer;
  position: relative;
  display: inline-block;
`;

const A = styled.button`
  background: none!important;
  border: none;
  padding: 0!important;
  color: #069;
  text-decoration: underline;
  cursor: pointer;

  vertical-align: middle;
  font-size: 17px;
  line-height: 22px;
  white-space: nowrap;

  -webkit-transition: color 0.2s ease-out;
  -moz-transition: color 0.2s ease-out;
  transition: color 0.2s ease-out;

  text-transform: uppercase;
  font-weight: 400;
  letter-spacing: 0.15em;

  ${props =>
    props.disabled &&
    `
    pointer-events: none;
    cursor: default;
    `};
  color: ${colors.middleDark}
    ${props =>
      props.isChosen &&
      `
    pointer-events: none;
    color: ${colors.light};
  `};

  &:hover,
  &:focus {
    text-decoration: none;
    color: ${colors.light};
    ${props => props.isChosen && "cursor: default;"};
  }
`;

const ActivatePremium = styled.span`
  font-family: ${fonts.mainTitle};
  background-color: rgba(255, 255, 255, 0.85);
  color: ${colors.light};
  font-size: 10px;
  border: 1px solid ${colors.light};
  padding: 3px;
  padding-bottom: 1px;
  line-height: 9px;
  border-radius: 3px;
  transform: rotate(-9deg);
  right: -17px;
  top: 12px;
  display: block;
  position: absolute;
  ${props => (props.commingSoon ? "right: -2px; top: 20px;" : "")};
`;
