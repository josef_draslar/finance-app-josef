import React from "react";
import PropTypes from "prop-types";
import {Modal} from "reactstrap";
import { connect } from "react-redux";
import styled from "styled-components";

import LogIn from "./LogIn";
import SignUp from "./SignUp";
import { fetchAndLogInUser } from "../redux/actions/UserActions";
import { fireLocalAction } from "../redux/actions/LocalActions";
import {
  CHANGE_PASSWORD,
  RESET_PASSWORD,
  SIGN_IN,
  SIGN_UP
} from "../redux/reducers/UserReducer";
import ResetPassword from "./ResetPassword";
import ResetPasswordChange from "./ResetPasswordChange";
import {localActions} from "../redux/reducers/LocalReducer";
import {media} from "../utils/Media";
import colors from "../utils/colors";

class Join extends React.PureComponent {
  state = {
    screen: this.props.joinModal || SIGN_IN
  };

  static getDerivedStateFromProps(props, state) {
    if (state.index !== props.index) {
      return {
        ...state,
        remaining: props.initial,
        index: props.index,
        updatedByProps: true
      };
    }
  }

  openPasswordReset = () => this.setState({ screen: RESET_PASSWORD });

  openSignUp = () => this.setState({ screen: SIGN_UP });

  openSignIn = () => this.setState({ screen: SIGN_IN });

  getContent = () => {
    const { fetchAndLogInUser } = this.props;
    switch (this.state.screen) {
      case SIGN_IN:
        return (
          <LogIn
            swap={this.openSignUp}
            close={this.closeLoggedIn}
            success={fetchAndLogInUser}
            openPasswordReset={this.openPasswordReset}
          />
        );
      case SIGN_UP:
        return <SignUp swap={this.openSignIn} close={this.closeNotLoggedIn} />;
      case RESET_PASSWORD:
        return <ResetPassword close={this.closeNotLoggedIn} />;
      case CHANGE_PASSWORD:
        return <ResetPasswordChange close={this.closeNotLoggedIn} />;
      default:
        return null;
    }
  };

  closeLoggedIn = () =>
    this.props.manualCloseJoin
      ? this.props.manualCloseJoin(true)
      : this.props.fireLocalAction(localActions.CLOSE_JOIN);

  closeNotLoggedIn = () =>
    this.props.manualCloseJoin
      ? this.props.manualCloseJoin()
      : this.props.fireLocalAction(localActions.CLOSE_JOIN);

  render() {
    return (
      <ModalStyled isOpen={true} toggle={this.closeNotLoggedIn}>
        {this.getContent()}
      </ModalStyled>
    );
  }
}

Join.propTypes = {
  manualCloseJoin: PropTypes.bool
};

class JoinWrapper extends React.PureComponent {
  render() {
    const { joinModal, manualCloseJoin } = this.props;

    if (!joinModal && !manualCloseJoin) return null;

    return <Join {...this.props} />;
  }
}

export default connect(
  state => ({
    joinModal: state.local.joinModal
  }),
  {
      fireLocalAction,
    fetchAndLogInUser
  }
)(JoinWrapper);

export const ModalStyled = styled(Modal)`
  margin: 80px auto 30px auto;
  width: 750px;
  max-width: 80%;
  background-color: ${colors.white};
  padding: 32px;
  position: relative;

  &:hover {
    cursor: default;
  }

  .modal-content {
    border: none;
  }

  ${media.lessThan("sm")`
      max-width: 90%;
      margin-top: 20px;
    `};
`;