import React from "react";
import PropTypes from "prop-types";
import axios from "../utils/Axios";
import styled from "styled-components";
import { Form, FormFeedback, FormGroup, Input } from "reactstrap";
import Button from "./Button";
import {
  ButtonWrapper,
  LabelLocal,
  ModalHeader
} from "./ModalJoinElements";
import { pushNotificationMessageIfPossible } from "./NotificationMessage";

//TODO finish implementaion
class ResetPassword extends React.Component {
  state = {
    email: "",
    isLoading: false,
    errors: {}
  };

  onChange = e => {
    const { name, value } = e.target;

    this.setState({
      [name]: value,
      errors: {
        ...this.state.errors,
        [name]: null
      }
    });
  };

  onSubmit = e => {
    e.preventDefault();
    const { email } = this.state;
    const { close } = this.props;
    this.setState({ isLoading: true });
    axios
      .post("/api/Users/resetPasswordRequest", {
        email
      })
      .then(() => {
        this.setState({ isLoading: false });
        close();
        pushNotificationMessageIfPossible({
          message: "Odkaz ke změně hesla byl zaslán na Váš email.",
          type: "SUCCESS"
        });
      })
      .catch(({ response }) => {
        if (
          response.data &&
          response.data.error &&
          response.data.error.details &&
          response.data.error.details.codes
        ) {
          this.setState({
            errors: response.data.error.details.codes,
            isLoading: false
          });
        }
      });
  };

  isDisabled = () => {
    const { email } = this.state;

    return !email;
  };

  render() {
    const { email, isLoading } = this.state;
    return (
      <div>
        <ModalHeader toggle={this.props.close}>Obnovení hesla</ModalHeader>
        <div className="row">
          <EmailWrapper>
            <Form onSubmit={this.onSubmit}>
              <FormGroup>
                <LabelLocal htmlFor="email-field">EMAIL</LabelLocal>
                <Input
                  id="email-field"
                  type="text"
                  name="email"
                  value={email}
                  onChange={this.onChange}
                  invalid={!!this.state.errors.email}
                />
                <FormFeedback>
                  Email je již zabraný, vyberte prosím jiný, nebo se přihlašte.
                </FormFeedback>
              </FormGroup>
              <ButtonWrapper>
                <Button
                  disabled={this.isDisabled()}
                  isLoading={isLoading}
                  type="submit"
                  isButton
                >
                  Resetuj heslo
                </Button>
              </ButtonWrapper>
            </Form>
          </EmailWrapper>
        </div>
      </div>
    );
  }
}

ResetPassword.propTypes = {
  close: PropTypes.func.isRequired
};

export default ResetPassword;

export const EmailWrapper = styled.div`
  margin-right: auto;
  margin-left: auto;
  max-width: 280px;
`;
