import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

class VimeoPlayer extends React.PureComponent {
  render() {
    return (
      <Div>
        <iframe
          src={"https://player.vimeo.com/video/" + this.props.id}
          width="640"
          height="360"
          frameBorder="0"
          allow="fullscreen"
          allowFullScreen
        />
      </Div>
    );
  }
}

VimeoPlayer.propTypes = {
  id: PropTypes.string.isRequired
};

export default VimeoPlayer;

const Div = styled.div`
  position: relative;
  padding-bottom: 56.25%;
  height: 0;
  overflow: hidden;
  max-width: 100%;
  height: auto;

  iframe {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
  }
`;
