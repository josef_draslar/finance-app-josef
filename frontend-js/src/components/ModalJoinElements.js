import styled from "styled-components";
import fonts from "../utils/fonts";
import colors from "../utils/colors";
import { media } from "../utils/Media";
import { Label, Input, ModalHeader as ModalHeaderBoot } from "reactstrap";
import {ButtonLikeLink} from "./BasicElements";

export const H1 = styled.h1`
  text-transform: uppercase;
  font-family: ${fonts.mainTitle};
  text-align: center;
`;

export const Text = styled.div`
  font-size: 18px;
  text-align: center;
  margin-top: 30px;
`;

export const FacebookButtonWrapper = styled.span`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translateY(-50%) translateX(-50%);

  ${media.lessThan("mobileLayout")`
    position: relative;
    top: unset;
    left: unset;
    width: 100%;
    display: block;
    transform: none;
    text-align: center;
  `};
`;

export const AOnClick = styled(ButtonLikeLink)`
  color: ${props => (props.inTour ? colors.white : colors.light)};
  ${props =>
    props.inTour &&
    `
    font-weight: 600;
  `} &:hover,
  &:focus {
    color: ${props => (props.inTour ? colors.white : colors.lightHover)};
  }
`;

export const A = styled.a`
  color: ${props => (props.inTour ? colors.white : colors.light)};
  ${props =>
    props.inTour &&
    `
    font-weight: 600;
  `} &:hover,
  &:focus {
    color: ${props => (props.inTour ? colors.white : colors.lightHover)};
  }
`;

export const SmallText = styled.div`
  font-size: 10px;
  text-align: center;
  margin-top: 30px;
`;

export const I = styled.i`
  padding-right: 14px;
  border-right: 1px solid ${colors.white};
  margin-right: 13px;
`;

export const EmailWrapper = styled.div`
  padding-left: 75px;
  margin-left: -15px;
  max-width: 280px;
  border-left: 1px solid
    ${props => (props.inTour ? colors.white : colors.lightGray)};
  position: relative;

  ${media.lessThan("mobileLayout")`
    border-left: none;
    border-top: 1px solid ${props =>
      props.inTour ? colors.white : colors.lightGray};
    padding-left: 0;
    margin-left: 0;
    margin-top: 30px;
    padding-top: 20px;
    max-width: unset;
  `};
`;

export const Or = styled.div`
  position: absolute;
  top: 50%;
  left: 0;
  color: ${props => (props.inTour ? colors.white : colors.lightGray)};
  background-color: ${props => (props.inTour ? colors.light : colors.white)};
  transform: translateY(-50%) translateX(-50%);
  font-family: ${fonts.mainTitle};

  ${media.lessThan("mobileLayout")`
    top: -12px;
    left: 50%;
    transform: translateX(-50%);
    padding-left: 10px;
    padding-right: 10px;
  `};
`;

export const LabelLocal = styled(Label)`
  font-family: ${fonts.mainTitle};
  margin-bottom: 5px;
`;

export const LabelCheckbox = styled(Label)`
  font-family: ${fonts.title};
  margin-bottom: 5px;
  font-size: 12px;
  margin-left: 25px;
`;

export const ButtonWrapper = styled.div`
  text-align: center;
`;

export const H6 = styled.h6`
  text-align: center;
  font-size: 16px;
  font-family: ${fonts.title};
`;

export const Checkbox = styled(Input)`
  margin-left: 0;
`;

export const SmallTextSpan = styled.span`
  border-top: 1px solid
    ${props => (props.inTour ? colors.white : colors.lightGray)};
  padding-top: 5px;
  margin-top: 20px;

  ${media.lessThan("sm")`
      display: block;
    `};
`;

export const ModalHeader = styled(ModalHeaderBoot)`
  padding-top: 0;
  border-bottom: none;

  h5 {
    text-transform: uppercase;
    font-family: ${fonts.mainTitle};
    font-size: 40px;
    font-weight: 500;
    width: 100%;
    text-align: center;
  }

  button {
    position: absolute;
    top: -15px;
    right: -15px;
    color: ${colors.dark};
  }
`;
