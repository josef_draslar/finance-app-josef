import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import Button from "./Button";
import colors from "../utils/colors";
import fonts from "../utils/fonts";

class ConfirmModal extends React.PureComponent {
  stopProp = e => e.stopPropagation();

  render() {
    const {
      showRemoveModal,
      confirmModalError,
      closeConfirmModalCb,
      processCb,
      processing,
      text
    } = this.props;
    if (showRemoveModal === null) return null;

    return (
      <ConfirmBackDrop onClick={closeConfirmModalCb}>
        <ConfirmBlock onClick={this.stopProp}>
          <ConfirmSpan>{text}</ConfirmSpan>
          {confirmModalError ? <div>{confirmModalError}</div> : ""}
          <div>
            <Button
              onClick={processCb}
              iconKey="check"
              isButton
              isLoading={processing}
            >
              Ano
            </Button>
            <Button
              style={{ marginLeft: "5px" }}
              onClick={closeConfirmModalCb}
              iconKey="times"
              inverseColors
              isButton
            >
              Ne
            </Button>
          </div>
        </ConfirmBlock>
      </ConfirmBackDrop>
    );
  }
}

ConfirmModal.propTypes = {
  showRemoveModal: PropTypes.bool.isRequired,
  confirmModalError: PropTypes.string,
  text: PropTypes.string.isRequired,
  closeConfirmModalCb: PropTypes.func,
  processCb: PropTypes.func.isRequired,
  processing: PropTypes.bool
};

export default ConfirmModal;

export const ConfirmBackDrop = styled.div`
  position: fixed;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  z-index: 200;

  &:hover {
    cursor: pointer;
  }
`;

export const ConfirmSpan = styled.span``;

export const ConfirmBlock = styled.div`
  position: fixed;
  top: 50%;
  transform: translateY(-50%);
  left: 50%;
  width: 300px;
  margin-left: -150px;
  padding: 15px;
  background-color: ${colors.white};
  text-transform: uppercase;
  font-size: 16px;
  font-family: ${fonts.text};
  font-weight: 600;

  ${ConfirmSpan} {
    margin-bottom: 20px;
    display: block;
  }

  &:hover {
    cursor: default;
  }

  -moz-box-shadow: 0 0 8px 0px rgba(0, 0, 0, 0.27);
  -webkit-box-shadow: 0 0 8px 0px rgba(0, 0, 0, 0.27);
  box-shadow: 0 0 8px 0px rgba(0, 0, 0, 0.27);
`;