import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

let notificationMessageCallback = null;

export const setNotificationMessageCallback = cb =>
  (notificationMessageCallback = cb);

export const pushNotificationMessageIfPossible = message => {
  if (!notificationMessageCallback) return;

  notificationMessageCallback(message);
  window.scrollTo(0, 0);
};

export const showNotificationMessage = messageObject => {
  if (messageObject) {
    const { title, message } = messageObject;
    return (
      <div className="alert alert-success" style={{ marginBottom: 0 }}>
        {title && <h6>{title}</h6>}
        {message}
      </div>
    );
  }
  return null;
};

class NotificationMessage extends React.PureComponent {
  state = {
    show: true
  };

  close = () => this.setState({ show: false });

  render() {
    const { messageObject } = this.props;
    if (!messageObject || !this.state.show) return null;

    const { title, message, type } = messageObject;
    return (
      <div
        style={{ position: "relative" }}
        className={"alert alert-" + (type === "ERROR" ? "danger" : "success")}
      >
        <CloseIcon className="fas fa-times" onClick={this.close} />
        {title && <h6>{title}</h6>}
        {message}
      </div>
    );
  }
}

NotificationMessage.propTypes = {
  messageObject: PropTypes.object
};

export default NotificationMessage;

const CloseIcon = styled.i`
  position: absolute;
  right: 10px;
  top: 3px;
  font-size: 14px;
`;
