import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import ReactGA from "react-ga";
import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";

import rootReducer from "./redux/reducers/RootReducer";
import Router from "./Router";
import registerServiceWorker from './utils/registerServiceWorker';
import "./index.css";
import {fetchTranslations} from "./utils/multiLanguageSupport";

ReactGA.initialize("UA-119018341-1");

fetchTranslations();

const store = createStore(rootReducer, {}, applyMiddleware(thunk));

ReactDOM.render(
    <Provider store={store}>
      <Router />
    </Provider>,
    document.getElementById("root")
);

registerServiceWorker();